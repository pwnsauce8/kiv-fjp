package main.java;

import main.java.compiler.enums.ErrorType;

public class SemanticError {
	private static final SemanticError instance = new SemanticError();
	
	private SemanticError() {
		
	}
	
	public static void printError(ErrorType error) {
		System.out.println("S_ERROR: " + error.getMsg());
		System.exit(1);
	}
	
	public SemanticError getInstance() {
		return instance;
	}
}
