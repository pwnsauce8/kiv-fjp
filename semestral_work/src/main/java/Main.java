package main.java;

import main.java.antlr4.OurJavaLexer;
import main.java.antlr4.OurJavaParser;
import main.java.compiler.model.Program;
import main.java.compiler.visitors.ProgramVisitor;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.model.Instruction;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        String input_path;
        String output_path;

        // Check input arguments <input file path> <output file path>
        if (args.length != 2)
        {
            usage();
            return;
        }

        input_path = args[0];
        output_path = args[1];

        // Read program
        String stream = getCharStream(input_path);
        runCompiler(stream);

        // Write instructions
        try {
            if (InstructionList.getInstance().getInstructionList() != null) {
                writeInstructions(output_path, InstructionList.getInstance().getInstructionList());
            }
        } catch (Exception ex) {
            // TODO: add errors
            System.out.println(ex.getMessage());
            return;
        }

    }

    /**
     * Method to parse program
     * @param input_program input program Char Stream
     * @throws IOException exceptions while parsing program
     */
    public static void runCompiler(String input_program) throws IOException {
        InputStream stream = new ByteArrayInputStream(input_program.getBytes(StandardCharsets.UTF_8));

        OurJavaLexer lexer = new OurJavaLexer(CharStreams.fromStream(stream, StandardCharsets.UTF_8));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        OurJavaParser parser = new OurJavaParser(tokens);

        parser.removeErrorListeners();

        // Process visitors
        Program program = null;
        try {
            program = new ProgramVisitor().visitProgram(parser.program());
        } catch (Exception ex) {
            // TODO: add errors
            System.out.println(ex.getMessage());
            return;
        }
    }

    private static String getCharStream(String input_path) throws IOException {
        Path program = Path.of(input_path);
        return Files.readString(program);
    }

    private static void writeInstructions(String outputFile, List<Instruction> instructions)
    {
        PrintWriter writer = null;
        try
        {
            writer = new PrintWriter(outputFile, "UTF-8");

            for (Instruction instruction: instructions)
            {
                writer.write(instruction.toString());
            }

            writer.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void usage()
    {
        System.out.println("+ ====================================================== +");
        System.out.println("| java -jar run.jar <input_file_path> <output_file_path> |");
        System.out.println("+ ====================================================== +");
    }
}
