// Generated from OurJava.g4 by ANTLR 4.9
package main.java.antlr4;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OurJavaLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BOOLEAN=1, INT=2, TRUE=3, FALSE=4, FINAL=5, IF=6, ELSE=7, SWITCH=8, CASE=9, 
		BREAK=10, FOR=11, DOWNTO=12, TO=13, WHILE=14, DO=15, REPEAT=16, UNTIL=17, 
		VOID=18, RETURN=19, OR=20, AND=21, SAME=22, NOT_EQ=23, LT=24, GT=25, LE=26, 
		GE=27, PLUS=28, MINUS=29, MULT=30, DIV=31, EQ=32, QUESTION=33, COLON=34, 
		NEGATION=35, LPAREN=36, RPAREN=37, LBRACE=38, RBRACE=39, SEMI=40, COMMA=41, 
		IDENTIFIER=42, DecimalNumeral=43, WHITESPACE=44, COMMENT=45, LINE_COMMENT=46;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"BOOLEAN", "INT", "TRUE", "FALSE", "FINAL", "IF", "ELSE", "SWITCH", "CASE", 
			"BREAK", "FOR", "DOWNTO", "TO", "WHILE", "DO", "REPEAT", "UNTIL", "VOID", 
			"RETURN", "OR", "AND", "SAME", "NOT_EQ", "LT", "GT", "LE", "GE", "PLUS", 
			"MINUS", "MULT", "DIV", "EQ", "QUESTION", "COLON", "NEGATION", "LPAREN", 
			"RPAREN", "LBRACE", "RBRACE", "SEMI", "COMMA", "IDENTIFIER", "ZERO", 
			"DecimalNumeral", "Digits", "Digit", "NonZeroDigit", "DigitsAndUnderscores", 
			"DigitOrUnderscore", "Underscores", "HexDigits", "HexDigit", "HexDigitsAndUnderscores", 
			"HexDigitOrUnderscore", "Letter", "Letter_or_digit", "WHITESPACE", "COMMENT", 
			"LINE_COMMENT"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'boolean'", "'int'", "'true'", "'false'", "'final'", "'if'", "'else'", 
			"'switch'", "'case'", "'break'", "'for'", "'downto'", "'to'", "'while'", 
			"'do'", "'repeat'", "'until'", "'void'", "'return'", "'||'", "'&&'", 
			"'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", 
			"'='", "'?'", "':'", "'!'", "'('", "')'", "'{'", "'}'", "';'", "','"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "BOOLEAN", "INT", "TRUE", "FALSE", "FINAL", "IF", "ELSE", "SWITCH", 
			"CASE", "BREAK", "FOR", "DOWNTO", "TO", "WHILE", "DO", "REPEAT", "UNTIL", 
			"VOID", "RETURN", "OR", "AND", "SAME", "NOT_EQ", "LT", "GT", "LE", "GE", 
			"PLUS", "MINUS", "MULT", "DIV", "EQ", "QUESTION", "COLON", "NEGATION", 
			"LPAREN", "RPAREN", "LBRACE", "RBRACE", "SEMI", "COMMA", "IDENTIFIER", 
			"DecimalNumeral", "WHITESPACE", "COMMENT", "LINE_COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public OurJavaLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "OurJava.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\60\u0178\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b"+
		"\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3"+
		"\16\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3"+
		"\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\26\3\27\3"+
		"\27\3\27\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3"+
		"\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3"+
		"%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\7+\u0115\n+\f+\16+\u0118\13+\3"+
		",\3,\3-\3-\3-\5-\u011f\n-\3-\3-\3-\5-\u0124\n-\5-\u0126\n-\3.\3.\5.\u012a"+
		"\n.\3.\5.\u012d\n.\3/\3/\5/\u0131\n/\3\60\3\60\3\61\6\61\u0136\n\61\r"+
		"\61\16\61\u0137\3\62\3\62\5\62\u013c\n\62\3\63\6\63\u013f\n\63\r\63\16"+
		"\63\u0140\3\64\3\64\5\64\u0145\n\64\3\64\5\64\u0148\n\64\3\65\3\65\3\66"+
		"\6\66\u014d\n\66\r\66\16\66\u014e\3\67\3\67\5\67\u0153\n\67\38\38\39\3"+
		"9\3:\6:\u015a\n:\r:\16:\u015b\3:\3:\3;\3;\3;\3;\7;\u0164\n;\f;\16;\u0167"+
		"\13;\3;\3;\3;\3;\3;\3<\3<\3<\3<\7<\u0172\n<\f<\16<\u0175\13<\3<\3<\3\u0165"+
		"\2=\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36"+
		";\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W\2Y-[\2]\2_\2a\2c\2e\2g\2i\2k\2m\2o\2"+
		"q\2s.u/w\60\3\2\b\3\2\63;\5\2\62;CHch\6\2&&C\\aac|\7\2&&\62;C\\aac|\5"+
		"\2\13\f\16\17\"\"\4\2\f\f\17\17\2\u017b\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3"+
		"\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2"+
		"\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35"+
		"\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)"+
		"\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2"+
		"\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2"+
		"A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3"+
		"\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2Y\3\2\2\2\2s\3\2\2"+
		"\2\2u\3\2\2\2\2w\3\2\2\2\3y\3\2\2\2\5\u0081\3\2\2\2\7\u0085\3\2\2\2\t"+
		"\u008a\3\2\2\2\13\u0090\3\2\2\2\r\u0096\3\2\2\2\17\u0099\3\2\2\2\21\u009e"+
		"\3\2\2\2\23\u00a5\3\2\2\2\25\u00aa\3\2\2\2\27\u00b0\3\2\2\2\31\u00b4\3"+
		"\2\2\2\33\u00bb\3\2\2\2\35\u00be\3\2\2\2\37\u00c4\3\2\2\2!\u00c7\3\2\2"+
		"\2#\u00ce\3\2\2\2%\u00d4\3\2\2\2\'\u00d9\3\2\2\2)\u00e0\3\2\2\2+\u00e3"+
		"\3\2\2\2-\u00e6\3\2\2\2/\u00e9\3\2\2\2\61\u00ec\3\2\2\2\63\u00ee\3\2\2"+
		"\2\65\u00f0\3\2\2\2\67\u00f3\3\2\2\29\u00f6\3\2\2\2;\u00f8\3\2\2\2=\u00fa"+
		"\3\2\2\2?\u00fc\3\2\2\2A\u00fe\3\2\2\2C\u0100\3\2\2\2E\u0102\3\2\2\2G"+
		"\u0104\3\2\2\2I\u0106\3\2\2\2K\u0108\3\2\2\2M\u010a\3\2\2\2O\u010c\3\2"+
		"\2\2Q\u010e\3\2\2\2S\u0110\3\2\2\2U\u0112\3\2\2\2W\u0119\3\2\2\2Y\u0125"+
		"\3\2\2\2[\u0127\3\2\2\2]\u0130\3\2\2\2_\u0132\3\2\2\2a\u0135\3\2\2\2c"+
		"\u013b\3\2\2\2e\u013e\3\2\2\2g\u0142\3\2\2\2i\u0149\3\2\2\2k\u014c\3\2"+
		"\2\2m\u0152\3\2\2\2o\u0154\3\2\2\2q\u0156\3\2\2\2s\u0159\3\2\2\2u\u015f"+
		"\3\2\2\2w\u016d\3\2\2\2yz\7d\2\2z{\7q\2\2{|\7q\2\2|}\7n\2\2}~\7g\2\2~"+
		"\177\7c\2\2\177\u0080\7p\2\2\u0080\4\3\2\2\2\u0081\u0082\7k\2\2\u0082"+
		"\u0083\7p\2\2\u0083\u0084\7v\2\2\u0084\6\3\2\2\2\u0085\u0086\7v\2\2\u0086"+
		"\u0087\7t\2\2\u0087\u0088\7w\2\2\u0088\u0089\7g\2\2\u0089\b\3\2\2\2\u008a"+
		"\u008b\7h\2\2\u008b\u008c\7c\2\2\u008c\u008d\7n\2\2\u008d\u008e\7u\2\2"+
		"\u008e\u008f\7g\2\2\u008f\n\3\2\2\2\u0090\u0091\7h\2\2\u0091\u0092\7k"+
		"\2\2\u0092\u0093\7p\2\2\u0093\u0094\7c\2\2\u0094\u0095\7n\2\2\u0095\f"+
		"\3\2\2\2\u0096\u0097\7k\2\2\u0097\u0098\7h\2\2\u0098\16\3\2\2\2\u0099"+
		"\u009a\7g\2\2\u009a\u009b\7n\2\2\u009b\u009c\7u\2\2\u009c\u009d\7g\2\2"+
		"\u009d\20\3\2\2\2\u009e\u009f\7u\2\2\u009f\u00a0\7y\2\2\u00a0\u00a1\7"+
		"k\2\2\u00a1\u00a2\7v\2\2\u00a2\u00a3\7e\2\2\u00a3\u00a4\7j\2\2\u00a4\22"+
		"\3\2\2\2\u00a5\u00a6\7e\2\2\u00a6\u00a7\7c\2\2\u00a7\u00a8\7u\2\2\u00a8"+
		"\u00a9\7g\2\2\u00a9\24\3\2\2\2\u00aa\u00ab\7d\2\2\u00ab\u00ac\7t\2\2\u00ac"+
		"\u00ad\7g\2\2\u00ad\u00ae\7c\2\2\u00ae\u00af\7m\2\2\u00af\26\3\2\2\2\u00b0"+
		"\u00b1\7h\2\2\u00b1\u00b2\7q\2\2\u00b2\u00b3\7t\2\2\u00b3\30\3\2\2\2\u00b4"+
		"\u00b5\7f\2\2\u00b5\u00b6\7q\2\2\u00b6\u00b7\7y\2\2\u00b7\u00b8\7p\2\2"+
		"\u00b8\u00b9\7v\2\2\u00b9\u00ba\7q\2\2\u00ba\32\3\2\2\2\u00bb\u00bc\7"+
		"v\2\2\u00bc\u00bd\7q\2\2\u00bd\34\3\2\2\2\u00be\u00bf\7y\2\2\u00bf\u00c0"+
		"\7j\2\2\u00c0\u00c1\7k\2\2\u00c1\u00c2\7n\2\2\u00c2\u00c3\7g\2\2\u00c3"+
		"\36\3\2\2\2\u00c4\u00c5\7f\2\2\u00c5\u00c6\7q\2\2\u00c6 \3\2\2\2\u00c7"+
		"\u00c8\7t\2\2\u00c8\u00c9\7g\2\2\u00c9\u00ca\7r\2\2\u00ca\u00cb\7g\2\2"+
		"\u00cb\u00cc\7c\2\2\u00cc\u00cd\7v\2\2\u00cd\"\3\2\2\2\u00ce\u00cf\7w"+
		"\2\2\u00cf\u00d0\7p\2\2\u00d0\u00d1\7v\2\2\u00d1\u00d2\7k\2\2\u00d2\u00d3"+
		"\7n\2\2\u00d3$\3\2\2\2\u00d4\u00d5\7x\2\2\u00d5\u00d6\7q\2\2\u00d6\u00d7"+
		"\7k\2\2\u00d7\u00d8\7f\2\2\u00d8&\3\2\2\2\u00d9\u00da\7t\2\2\u00da\u00db"+
		"\7g\2\2\u00db\u00dc\7v\2\2\u00dc\u00dd\7w\2\2\u00dd\u00de\7t\2\2\u00de"+
		"\u00df\7p\2\2\u00df(\3\2\2\2\u00e0\u00e1\7~\2\2\u00e1\u00e2\7~\2\2\u00e2"+
		"*\3\2\2\2\u00e3\u00e4\7(\2\2\u00e4\u00e5\7(\2\2\u00e5,\3\2\2\2\u00e6\u00e7"+
		"\7?\2\2\u00e7\u00e8\7?\2\2\u00e8.\3\2\2\2\u00e9\u00ea\7#\2\2\u00ea\u00eb"+
		"\7?\2\2\u00eb\60\3\2\2\2\u00ec\u00ed\7>\2\2\u00ed\62\3\2\2\2\u00ee\u00ef"+
		"\7@\2\2\u00ef\64\3\2\2\2\u00f0\u00f1\7>\2\2\u00f1\u00f2\7?\2\2\u00f2\66"+
		"\3\2\2\2\u00f3\u00f4\7@\2\2\u00f4\u00f5\7?\2\2\u00f58\3\2\2\2\u00f6\u00f7"+
		"\7-\2\2\u00f7:\3\2\2\2\u00f8\u00f9\7/\2\2\u00f9<\3\2\2\2\u00fa\u00fb\7"+
		",\2\2\u00fb>\3\2\2\2\u00fc\u00fd\7\61\2\2\u00fd@\3\2\2\2\u00fe\u00ff\7"+
		"?\2\2\u00ffB\3\2\2\2\u0100\u0101\7A\2\2\u0101D\3\2\2\2\u0102\u0103\7<"+
		"\2\2\u0103F\3\2\2\2\u0104\u0105\7#\2\2\u0105H\3\2\2\2\u0106\u0107\7*\2"+
		"\2\u0107J\3\2\2\2\u0108\u0109\7+\2\2\u0109L\3\2\2\2\u010a\u010b\7}\2\2"+
		"\u010bN\3\2\2\2\u010c\u010d\7\177\2\2\u010dP\3\2\2\2\u010e\u010f\7=\2"+
		"\2\u010fR\3\2\2\2\u0110\u0111\7.\2\2\u0111T\3\2\2\2\u0112\u0116\5o8\2"+
		"\u0113\u0115\5q9\2\u0114\u0113\3\2\2\2\u0115\u0118\3\2\2\2\u0116\u0114"+
		"\3\2\2\2\u0116\u0117\3\2\2\2\u0117V\3\2\2\2\u0118\u0116\3\2\2\2\u0119"+
		"\u011a\7\62\2\2\u011aX\3\2\2\2\u011b\u0126\5W,\2\u011c\u0123\5_\60\2\u011d"+
		"\u011f\5[.\2\u011e\u011d\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u0124\3\2\2"+
		"\2\u0120\u0121\5e\63\2\u0121\u0122\5[.\2\u0122\u0124\3\2\2\2\u0123\u011e"+
		"\3\2\2\2\u0123\u0120\3\2\2\2\u0124\u0126\3\2\2\2\u0125\u011b\3\2\2\2\u0125"+
		"\u011c\3\2\2\2\u0126Z\3\2\2\2\u0127\u012c\5]/\2\u0128\u012a\5a\61\2\u0129"+
		"\u0128\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u012d\5]"+
		"/\2\u012c\u0129\3\2\2\2\u012c\u012d\3\2\2\2\u012d\\\3\2\2\2\u012e\u0131"+
		"\5W,\2\u012f\u0131\5_\60\2\u0130\u012e\3\2\2\2\u0130\u012f\3\2\2\2\u0131"+
		"^\3\2\2\2\u0132\u0133\t\2\2\2\u0133`\3\2\2\2\u0134\u0136\5c\62\2\u0135"+
		"\u0134\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0135\3\2\2\2\u0137\u0138\3\2"+
		"\2\2\u0138b\3\2\2\2\u0139\u013c\5]/\2\u013a\u013c\7a\2\2\u013b\u0139\3"+
		"\2\2\2\u013b\u013a\3\2\2\2\u013cd\3\2\2\2\u013d\u013f\7a\2\2\u013e\u013d"+
		"\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141"+
		"f\3\2\2\2\u0142\u0147\5i\65\2\u0143\u0145\5k\66\2\u0144\u0143\3\2\2\2"+
		"\u0144\u0145\3\2\2\2\u0145\u0146\3\2\2\2\u0146\u0148\5i\65\2\u0147\u0144"+
		"\3\2\2\2\u0147\u0148\3\2\2\2\u0148h\3\2\2\2\u0149\u014a\t\3\2\2\u014a"+
		"j\3\2\2\2\u014b\u014d\5m\67\2\u014c\u014b\3\2\2\2\u014d\u014e\3\2\2\2"+
		"\u014e\u014c\3\2\2\2\u014e\u014f\3\2\2\2\u014fl\3\2\2\2\u0150\u0153\5"+
		"i\65\2\u0151\u0153\7a\2\2\u0152\u0150\3\2\2\2\u0152\u0151\3\2\2\2\u0153"+
		"n\3\2\2\2\u0154\u0155\t\4\2\2\u0155p\3\2\2\2\u0156\u0157\t\5\2\2\u0157"+
		"r\3\2\2\2\u0158\u015a\t\6\2\2\u0159\u0158\3\2\2\2\u015a\u015b\3\2\2\2"+
		"\u015b\u0159\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u015e"+
		"\b:\2\2\u015et\3\2\2\2\u015f\u0160\7\61\2\2\u0160\u0161\7,\2\2\u0161\u0165"+
		"\3\2\2\2\u0162\u0164\13\2\2\2\u0163\u0162\3\2\2\2\u0164\u0167\3\2\2\2"+
		"\u0165\u0166\3\2\2\2\u0165\u0163\3\2\2\2\u0166\u0168\3\2\2\2\u0167\u0165"+
		"\3\2\2\2\u0168\u0169\7,\2\2\u0169\u016a\7\61\2\2\u016a\u016b\3\2\2\2\u016b"+
		"\u016c\b;\2\2\u016cv\3\2\2\2\u016d\u016e\7\61\2\2\u016e\u016f\7\61\2\2"+
		"\u016f\u0173\3\2\2\2\u0170\u0172\n\7\2\2\u0171\u0170\3\2\2\2\u0172\u0175"+
		"\3\2\2\2\u0173\u0171\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0176\3\2\2\2\u0175"+
		"\u0173\3\2\2\2\u0176\u0177\b<\2\2\u0177x\3\2\2\2\24\2\u0116\u011e\u0123"+
		"\u0125\u0129\u012c\u0130\u0137\u013b\u0140\u0144\u0147\u014e\u0152\u015b"+
		"\u0165\u0173\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}