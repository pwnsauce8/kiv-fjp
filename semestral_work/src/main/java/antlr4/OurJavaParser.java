// Generated from OurJava.g4 by ANTLR 4.9
package main.java.antlr4;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class OurJavaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BOOLEAN=1, INT=2, TRUE=3, FALSE=4, FINAL=5, IF=6, ELSE=7, SWITCH=8, CASE=9, 
		BREAK=10, FOR=11, DOWNTO=12, TO=13, WHILE=14, DO=15, REPEAT=16, UNTIL=17, 
		VOID=18, RETURN=19, OR=20, AND=21, SAME=22, NOT_EQ=23, LT=24, GT=25, LE=26, 
		GE=27, PLUS=28, MINUS=29, MULT=30, DIV=31, EQ=32, QUESTION=33, COLON=34, 
		NEGATION=35, LPAREN=36, RPAREN=37, LBRACE=38, RBRACE=39, SEMI=40, COMMA=41, 
		IDENTIFIER=42, DecimalNumeral=43, WHITESPACE=44, COMMENT=45, LINE_COMMENT=46;
	public static final int
		RULE_values = 0, RULE_boolean_values = 1, RULE_decimal_values = 2, RULE_varTypes = 3, 
		RULE_decimal_symbol = 4, RULE_modifier = 5, RULE_program = 6, RULE_block = 7, 
		RULE_body = 8, RULE_block_statement = 9, RULE_statements = 10, RULE_block_body = 11, 
		RULE_switch_body = 12, RULE_switch_body_statement = 13, RULE_statement = 14, 
		RULE_variable_assigment = 15, RULE_return_statement = 16, RULE_expression_body = 17, 
		RULE_variable_declaration = 18, RULE_decimal_variable = 19, RULE_bool_variable = 20, 
		RULE_parallel_declaration = 21, RULE_expression = 22, RULE_literal = 23, 
		RULE_boolean_literal = 24, RULE_integer_literal = 25, RULE_for_type = 26, 
		RULE_for_init = 27, RULE_for_statement = 28, RULE_switch_block = 29, RULE_function_declaration = 30, 
		RULE_function_header = 31, RULE_function_declarator = 32, RULE_function_type = 33, 
		RULE_function_body = 34, RULE_formal_parameter = 35, RULE_function_call_statement = 36, 
		RULE_argument_list = 37;
	private static String[] makeRuleNames() {
		return new String[] {
			"values", "boolean_values", "decimal_values", "varTypes", "decimal_symbol", 
			"modifier", "program", "block", "body", "block_statement", "statements", 
			"block_body", "switch_body", "switch_body_statement", "statement", "variable_assigment", 
			"return_statement", "expression_body", "variable_declaration", "decimal_variable", 
			"bool_variable", "parallel_declaration", "expression", "literal", "boolean_literal", 
			"integer_literal", "for_type", "for_init", "for_statement", "switch_block", 
			"function_declaration", "function_header", "function_declarator", "function_type", 
			"function_body", "formal_parameter", "function_call_statement", "argument_list"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'boolean'", "'int'", "'true'", "'false'", "'final'", "'if'", "'else'", 
			"'switch'", "'case'", "'break'", "'for'", "'downto'", "'to'", "'while'", 
			"'do'", "'repeat'", "'until'", "'void'", "'return'", "'||'", "'&&'", 
			"'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", 
			"'='", "'?'", "':'", "'!'", "'('", "')'", "'{'", "'}'", "';'", "','"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "BOOLEAN", "INT", "TRUE", "FALSE", "FINAL", "IF", "ELSE", "SWITCH", 
			"CASE", "BREAK", "FOR", "DOWNTO", "TO", "WHILE", "DO", "REPEAT", "UNTIL", 
			"VOID", "RETURN", "OR", "AND", "SAME", "NOT_EQ", "LT", "GT", "LE", "GE", 
			"PLUS", "MINUS", "MULT", "DIV", "EQ", "QUESTION", "COLON", "NEGATION", 
			"LPAREN", "RPAREN", "LBRACE", "RBRACE", "SEMI", "COMMA", "IDENTIFIER", 
			"DecimalNumeral", "WHITESPACE", "COMMENT", "LINE_COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "OurJava.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public OurJavaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ValuesContext extends ParserRuleContext {
		public Boolean_valuesContext boolean_values() {
			return getRuleContext(Boolean_valuesContext.class,0);
		}
		public Decimal_valuesContext decimal_values() {
			return getRuleContext(Decimal_valuesContext.class,0);
		}
		public ValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValuesContext values() throws RecognitionException {
		ValuesContext _localctx = new ValuesContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_values);
		try {
			setState(78);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
			case FALSE:
				enterOuterAlt(_localctx, 1);
				{
				setState(76);
				boolean_values();
				}
				break;
			case PLUS:
			case MINUS:
			case DecimalNumeral:
				enterOuterAlt(_localctx, 2);
				{
				setState(77);
				decimal_values();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Boolean_valuesContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(OurJavaParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(OurJavaParser.FALSE, 0); }
		public Boolean_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolean_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBoolean_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBoolean_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBoolean_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Boolean_valuesContext boolean_values() throws RecognitionException {
		Boolean_valuesContext _localctx = new Boolean_valuesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_boolean_values);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Decimal_valuesContext extends ParserRuleContext {
		public TerminalNode DecimalNumeral() { return getToken(OurJavaParser.DecimalNumeral, 0); }
		public Decimal_symbolContext decimal_symbol() {
			return getRuleContext(Decimal_symbolContext.class,0);
		}
		public Decimal_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimal_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterDecimal_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitDecimal_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitDecimal_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Decimal_valuesContext decimal_values() throws RecognitionException {
		Decimal_valuesContext _localctx = new Decimal_valuesContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_decimal_values);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PLUS || _la==MINUS) {
				{
				setState(82);
				decimal_symbol();
				}
			}

			setState(85);
			match(DecimalNumeral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarTypesContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(OurJavaParser.INT, 0); }
		public TerminalNode BOOLEAN() { return getToken(OurJavaParser.BOOLEAN, 0); }
		public VarTypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varTypes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterVarTypes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitVarTypes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitVarTypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarTypesContext varTypes() throws RecognitionException {
		VarTypesContext _localctx = new VarTypesContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_varTypes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			_la = _input.LA(1);
			if ( !(_la==BOOLEAN || _la==INT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Decimal_symbolContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(OurJavaParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(OurJavaParser.MINUS, 0); }
		public Decimal_symbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimal_symbol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterDecimal_symbol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitDecimal_symbol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitDecimal_symbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Decimal_symbolContext decimal_symbol() throws RecognitionException {
		Decimal_symbolContext _localctx = new Decimal_symbolContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_decimal_symbol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifierContext extends ParserRuleContext {
		public TerminalNode FINAL() { return getToken(OurJavaParser.FINAL, 0); }
		public ModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitModifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitModifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModifierContext modifier() throws RecognitionException {
		ModifierContext _localctx = new ModifierContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_modifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			match(FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode EOF() { return getToken(OurJavaParser.EOF, 0); }
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			block();
			setState(94);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(OurJavaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(OurJavaParser.RBRACE, 0); }
		public Block_statementContext block_statement() {
			return getRuleContext(Block_statementContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			match(LBRACE);
			setState(98);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << FINAL) | (1L << IF) | (1L << SWITCH) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << REPEAT) | (1L << VOID) | (1L << RETURN) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(97);
				block_statement();
				}
			}

			setState(100);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(OurJavaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(OurJavaParser.RBRACE, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			match(LBRACE);
			setState(104);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << FINAL) | (1L << IF) | (1L << SWITCH) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << REPEAT) | (1L << RETURN) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(103);
				block_body();
				}
			}

			setState(106);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_statementContext extends ParserRuleContext {
		public List<StatementsContext> statements() {
			return getRuleContexts(StatementsContext.class);
		}
		public StatementsContext statements(int i) {
			return getRuleContext(StatementsContext.class,i);
		}
		public Block_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBlock_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBlock_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBlock_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Block_statementContext block_statement() throws RecognitionException {
		Block_statementContext _localctx = new Block_statementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_block_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(108);
				statements();
				}
				}
				setState(111); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << FINAL) | (1L << IF) | (1L << SWITCH) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << REPEAT) | (1L << VOID) | (1L << RETURN) | (1L << IDENTIFIER))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementsContext extends ParserRuleContext {
		public Function_declarationContext function_declaration() {
			return getRuleContext(Function_declarationContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitStatements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitStatements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		StatementsContext _localctx = new StatementsContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_statements);
		try {
			setState(115);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(113);
				function_declaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				statement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_bodyContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Block_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBlock_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBlock_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBlock_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Block_bodyContext block_body() throws RecognitionException {
		Block_bodyContext _localctx = new Block_bodyContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_block_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(117);
				statement();
				}
				}
				setState(120); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << FINAL) | (1L << IF) | (1L << SWITCH) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << REPEAT) | (1L << RETURN) | (1L << IDENTIFIER))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_bodyContext extends ParserRuleContext {
		public List<Switch_body_statementContext> switch_body_statement() {
			return getRuleContexts(Switch_body_statementContext.class);
		}
		public Switch_body_statementContext switch_body_statement(int i) {
			return getRuleContext(Switch_body_statementContext.class,i);
		}
		public Switch_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterSwitch_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitSwitch_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitSwitch_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_bodyContext switch_body() throws RecognitionException {
		Switch_bodyContext _localctx = new Switch_bodyContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_switch_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << FINAL) | (1L << IF) | (1L << SWITCH) | (1L << BREAK) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << REPEAT) | (1L << RETURN) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(122);
				switch_body_statement();
				}
				}
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_body_statementContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public TerminalNode BREAK() { return getToken(OurJavaParser.BREAK, 0); }
		public TerminalNode SEMI() { return getToken(OurJavaParser.SEMI, 0); }
		public Switch_body_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_body_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterSwitch_body_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitSwitch_body_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitSwitch_body_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_body_statementContext switch_body_statement() throws RecognitionException {
		Switch_body_statementContext _localctx = new Switch_body_statementContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_switch_body_statement);
		try {
			setState(131);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BOOLEAN:
			case INT:
			case FINAL:
			case IF:
			case SWITCH:
			case FOR:
			case WHILE:
			case DO:
			case REPEAT:
			case RETURN:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(128);
				statement();
				}
				break;
			case BREAK:
				enterOuterAlt(_localctx, 2);
				{
				setState(129);
				match(BREAK);
				setState(130);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class WhileStatementContext extends StatementContext {
		public TerminalNode WHILE() { return getToken(OurJavaParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public WhileStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitWhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitWhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepeatuntilStatementContext extends StatementContext {
		public TerminalNode REPEAT() { return getToken(OurJavaParser.REPEAT, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode UNTIL() { return getToken(OurJavaParser.UNTIL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RepeatuntilStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterRepeatuntilStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitRepeatuntilStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitRepeatuntilStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DowhileStatementContext extends StatementContext {
		public TerminalNode DO() { return getToken(OurJavaParser.DO, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode WHILE() { return getToken(OurJavaParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DowhileStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterDowhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitDowhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitDowhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfelseStatementContext extends StatementContext {
		public TerminalNode IF() { return getToken(OurJavaParser.IF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<BodyContext> body() {
			return getRuleContexts(BodyContext.class);
		}
		public BodyContext body(int i) {
			return getRuleContext(BodyContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(OurJavaParser.ELSE, 0); }
		public IfelseStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterIfelseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitIfelseStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitIfelseStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarassigmentStatementContext extends StatementContext {
		public Variable_assigmentContext variable_assigment() {
			return getRuleContext(Variable_assigmentContext.class,0);
		}
		public VarassigmentStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterVarassigmentStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitVarassigmentStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitVarassigmentStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VardeclarationStatementContext extends StatementContext {
		public Variable_declarationContext variable_declaration() {
			return getRuleContext(Variable_declarationContext.class,0);
		}
		public VardeclarationStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterVardeclarationStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitVardeclarationStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitVardeclarationStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctioncallStatementContext extends StatementContext {
		public Function_call_statementContext function_call_statement() {
			return getRuleContext(Function_call_statementContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(OurJavaParser.SEMI, 0); }
		public FunctioncallStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunctioncallStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunctioncallStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunctioncallStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ForStatementContext extends StatementContext {
		public TerminalNode FOR() { return getToken(OurJavaParser.FOR, 0); }
		public For_statementContext for_statement() {
			return getRuleContext(For_statementContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public ForStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterForStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitForStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitForStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReturnStatementContext extends StatementContext {
		public Return_statementContext return_statement() {
			return getRuleContext(Return_statementContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(OurJavaParser.SEMI, 0); }
		public ReturnStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterReturnStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitReturnStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitReturnStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SwitchStatementContext extends StatementContext {
		public TerminalNode SWITCH() { return getToken(OurJavaParser.SWITCH, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode LBRACE() { return getToken(OurJavaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(OurJavaParser.RBRACE, 0); }
		public List<Switch_blockContext> switch_block() {
			return getRuleContexts(Switch_blockContext.class);
		}
		public Switch_blockContext switch_block(int i) {
			return getRuleContext(Switch_blockContext.class,i);
		}
		public SwitchStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterSwitchStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitSwitchStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitSwitchStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhiledoStatementContext extends StatementContext {
		public TerminalNode WHILE() { return getToken(OurJavaParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode DO() { return getToken(OurJavaParser.DO, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public WhiledoStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterWhiledoStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitWhiledoStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitWhiledoStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_statement);
		int _la;
		try {
			setState(182);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				_localctx = new IfelseStatementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(133);
				match(IF);
				setState(134);
				expression();
				setState(135);
				body();
				setState(138);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ELSE) {
					{
					setState(136);
					match(ELSE);
					setState(137);
					body();
					}
				}

				}
				break;
			case 2:
				_localctx = new WhileStatementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(140);
				match(WHILE);
				setState(141);
				expression();
				setState(142);
				body();
				}
				break;
			case 3:
				_localctx = new DowhileStatementContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(144);
				match(DO);
				setState(145);
				body();
				setState(146);
				match(WHILE);
				setState(147);
				expression();
				}
				break;
			case 4:
				_localctx = new WhiledoStatementContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(149);
				match(WHILE);
				setState(150);
				expression();
				setState(151);
				match(DO);
				setState(152);
				body();
				}
				break;
			case 5:
				_localctx = new RepeatuntilStatementContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(154);
				match(REPEAT);
				setState(155);
				body();
				setState(156);
				match(UNTIL);
				setState(157);
				expression();
				}
				break;
			case 6:
				_localctx = new ForStatementContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(159);
				match(FOR);
				setState(160);
				for_statement();
				setState(161);
				body();
				}
				break;
			case 7:
				_localctx = new SwitchStatementContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(163);
				match(SWITCH);
				setState(164);
				expression();
				setState(165);
				match(LBRACE);
				setState(169);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==CASE) {
					{
					{
					setState(166);
					switch_block();
					}
					}
					setState(171);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(172);
				match(RBRACE);
				}
				break;
			case 8:
				_localctx = new FunctioncallStatementContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(174);
				function_call_statement();
				setState(175);
				match(SEMI);
				}
				break;
			case 9:
				_localctx = new VarassigmentStatementContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(177);
				variable_assigment();
				}
				break;
			case 10:
				_localctx = new VardeclarationStatementContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(178);
				variable_declaration();
				}
				break;
			case 11:
				_localctx = new ReturnStatementContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(179);
				return_statement();
				setState(180);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_assigmentContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public TerminalNode EQ() { return getToken(OurJavaParser.EQ, 0); }
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(OurJavaParser.SEMI, 0); }
		public Variable_assigmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_assigment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterVariable_assigment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitVariable_assigment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitVariable_assigment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_assigmentContext variable_assigment() throws RecognitionException {
		Variable_assigmentContext _localctx = new Variable_assigmentContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_variable_assigment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			match(IDENTIFIER);
			setState(185);
			match(EQ);
			setState(186);
			expression_body(0);
			setState(187);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_statementContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(OurJavaParser.RETURN, 0); }
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public Return_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterReturn_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitReturn_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitReturn_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_statementContext return_statement() throws RecognitionException {
		Return_statementContext _localctx = new Return_statementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_return_statement);
		try {
			setState(192);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(189);
				match(RETURN);
				setState(190);
				expression_body(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(191);
				match(RETURN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_bodyContext extends ParserRuleContext {
		public Expression_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_body; }
	 
		public Expression_bodyContext() { }
		public void copyFrom(Expression_bodyContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NegationExpressionContext extends Expression_bodyContext {
		public TerminalNode NEGATION() { return getToken(OurJavaParser.NEGATION, 0); }
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public NegationExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterNegationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitNegationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitNegationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpbodyExpressionContext extends Expression_bodyContext {
		public TerminalNode LPAREN() { return getToken(OurJavaParser.LPAREN, 0); }
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(OurJavaParser.RPAREN, 0); }
		public ExpbodyExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterExpbodyExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitExpbodyExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitExpbodyExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunccallExpressionContext extends Expression_bodyContext {
		public Function_call_statementContext function_call_statement() {
			return getRuleContext(Function_call_statementContext.class,0);
		}
		public FunccallExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunccallExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunccallExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunccallExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RelationalExpressionContext extends Expression_bodyContext {
		public Token op;
		public List<Expression_bodyContext> expression_body() {
			return getRuleContexts(Expression_bodyContext.class);
		}
		public Expression_bodyContext expression_body(int i) {
			return getRuleContext(Expression_bodyContext.class,i);
		}
		public TerminalNode GT() { return getToken(OurJavaParser.GT, 0); }
		public TerminalNode GE() { return getToken(OurJavaParser.GE, 0); }
		public TerminalNode LT() { return getToken(OurJavaParser.LT, 0); }
		public TerminalNode LE() { return getToken(OurJavaParser.LE, 0); }
		public TerminalNode SAME() { return getToken(OurJavaParser.SAME, 0); }
		public TerminalNode NOT_EQ() { return getToken(OurJavaParser.NOT_EQ, 0); }
		public RelationalExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterRelationalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitRelationalExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitRelationalExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValuesExpressionContext extends Expression_bodyContext {
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public ValuesExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterValuesExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitValuesExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitValuesExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentifierExpressionContext extends Expression_bodyContext {
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public IdentifierExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterIdentifierExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitIdentifierExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitIdentifierExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlusandminusExpressionContext extends Expression_bodyContext {
		public Token op;
		public List<Expression_bodyContext> expression_body() {
			return getRuleContexts(Expression_bodyContext.class);
		}
		public Expression_bodyContext expression_body(int i) {
			return getRuleContext(Expression_bodyContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(OurJavaParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(OurJavaParser.MINUS, 0); }
		public PlusandminusExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterPlusandminusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitPlusandminusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitPlusandminusExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinusPlusExpressionContext extends Expression_bodyContext {
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(OurJavaParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(OurJavaParser.PLUS, 0); }
		public MinusPlusExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterMinusPlusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitMinusPlusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitMinusPlusExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LogicalExpressionContext extends Expression_bodyContext {
		public Token op;
		public List<Expression_bodyContext> expression_body() {
			return getRuleContexts(Expression_bodyContext.class);
		}
		public Expression_bodyContext expression_body(int i) {
			return getRuleContext(Expression_bodyContext.class,i);
		}
		public TerminalNode AND() { return getToken(OurJavaParser.AND, 0); }
		public TerminalNode OR() { return getToken(OurJavaParser.OR, 0); }
		public LogicalExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterLogicalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitLogicalExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitLogicalExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TernaryExpressionContext extends Expression_bodyContext {
		public List<Expression_bodyContext> expression_body() {
			return getRuleContexts(Expression_bodyContext.class);
		}
		public Expression_bodyContext expression_body(int i) {
			return getRuleContext(Expression_bodyContext.class,i);
		}
		public TerminalNode QUESTION() { return getToken(OurJavaParser.QUESTION, 0); }
		public TerminalNode COLON() { return getToken(OurJavaParser.COLON, 0); }
		public TernaryExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterTernaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitTernaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitTernaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultanddivExpressionContext extends Expression_bodyContext {
		public Token op;
		public List<Expression_bodyContext> expression_body() {
			return getRuleContexts(Expression_bodyContext.class);
		}
		public Expression_bodyContext expression_body(int i) {
			return getRuleContext(Expression_bodyContext.class,i);
		}
		public TerminalNode MULT() { return getToken(OurJavaParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(OurJavaParser.DIV, 0); }
		public MultanddivExpressionContext(Expression_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterMultanddivExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitMultanddivExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitMultanddivExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expression_bodyContext expression_body() throws RecognitionException {
		return expression_body(0);
	}

	private Expression_bodyContext expression_body(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expression_bodyContext _localctx = new Expression_bodyContext(_ctx, _parentState);
		Expression_bodyContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_expression_body, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				_localctx = new ValuesExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(195);
				values();
				}
				break;
			case 2:
				{
				_localctx = new IdentifierExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(196);
				match(IDENTIFIER);
				}
				break;
			case 3:
				{
				_localctx = new FunccallExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(197);
				function_call_statement();
				}
				break;
			case 4:
				{
				_localctx = new ExpbodyExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(198);
				match(LPAREN);
				setState(199);
				expression_body(0);
				setState(200);
				match(RPAREN);
				}
				break;
			case 5:
				{
				_localctx = new NegationExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(202);
				match(NEGATION);
				setState(203);
				expression_body(3);
				}
				break;
			case 6:
				{
				_localctx = new MinusPlusExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(204);
				_la = _input.LA(1);
				if ( !(_la==PLUS || _la==MINUS) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(205);
				expression_body(2);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(228);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(226);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
					case 1:
						{
						_localctx = new MultanddivExpressionContext(new Expression_bodyContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression_body);
						setState(208);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(209);
						((MultanddivExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==MULT || _la==DIV) ) {
							((MultanddivExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(210);
						expression_body(9);
						}
						break;
					case 2:
						{
						_localctx = new PlusandminusExpressionContext(new Expression_bodyContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression_body);
						setState(211);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(212);
						((PlusandminusExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
							((PlusandminusExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(213);
						expression_body(8);
						}
						break;
					case 3:
						{
						_localctx = new RelationalExpressionContext(new Expression_bodyContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression_body);
						setState(214);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(215);
						((RelationalExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SAME) | (1L << NOT_EQ) | (1L << LT) | (1L << GT) | (1L << LE) | (1L << GE))) != 0)) ) {
							((RelationalExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(216);
						expression_body(7);
						}
						break;
					case 4:
						{
						_localctx = new LogicalExpressionContext(new Expression_bodyContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression_body);
						setState(217);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(218);
						((LogicalExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==OR || _la==AND) ) {
							((LogicalExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(219);
						expression_body(6);
						}
						break;
					case 5:
						{
						_localctx = new TernaryExpressionContext(new Expression_bodyContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression_body);
						setState(220);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(221);
						match(QUESTION);
						setState(222);
						expression_body(0);
						setState(223);
						match(COLON);
						setState(224);
						expression_body(2);
						}
						break;
					}
					} 
				}
				setState(230);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Variable_declarationContext extends ParserRuleContext {
		public Decimal_variableContext decimal_variable() {
			return getRuleContext(Decimal_variableContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(OurJavaParser.SEMI, 0); }
		public Bool_variableContext bool_variable() {
			return getRuleContext(Bool_variableContext.class,0);
		}
		public Variable_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterVariable_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitVariable_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitVariable_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_declarationContext variable_declaration() throws RecognitionException {
		Variable_declarationContext _localctx = new Variable_declarationContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_variable_declaration);
		try {
			setState(237);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(231);
				decimal_variable();
				setState(232);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(234);
				bool_variable();
				setState(235);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Decimal_variableContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(OurJavaParser.INT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public TerminalNode EQ() { return getToken(OurJavaParser.EQ, 0); }
		public Integer_literalContext integer_literal() {
			return getRuleContext(Integer_literalContext.class,0);
		}
		public ModifierContext modifier() {
			return getRuleContext(ModifierContext.class,0);
		}
		public List<Parallel_declarationContext> parallel_declaration() {
			return getRuleContexts(Parallel_declarationContext.class);
		}
		public Parallel_declarationContext parallel_declaration(int i) {
			return getRuleContext(Parallel_declarationContext.class,i);
		}
		public Decimal_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimal_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterDecimal_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitDecimal_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitDecimal_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Decimal_variableContext decimal_variable() throws RecognitionException {
		Decimal_variableContext _localctx = new Decimal_variableContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_decimal_variable);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(240);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FINAL) {
				{
				setState(239);
				modifier();
				}
			}

			setState(242);
			match(INT);
			setState(243);
			match(IDENTIFIER);
			setState(247);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(244);
					parallel_declaration();
					}
					} 
				}
				setState(249);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			setState(250);
			match(EQ);
			setState(251);
			integer_literal();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bool_variableContext extends ParserRuleContext {
		public TerminalNode BOOLEAN() { return getToken(OurJavaParser.BOOLEAN, 0); }
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public TerminalNode EQ() { return getToken(OurJavaParser.EQ, 0); }
		public Boolean_literalContext boolean_literal() {
			return getRuleContext(Boolean_literalContext.class,0);
		}
		public ModifierContext modifier() {
			return getRuleContext(ModifierContext.class,0);
		}
		public List<Parallel_declarationContext> parallel_declaration() {
			return getRuleContexts(Parallel_declarationContext.class);
		}
		public Parallel_declarationContext parallel_declaration(int i) {
			return getRuleContext(Parallel_declarationContext.class,i);
		}
		public Bool_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBool_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBool_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBool_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bool_variableContext bool_variable() throws RecognitionException {
		Bool_variableContext _localctx = new Bool_variableContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_bool_variable);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(254);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FINAL) {
				{
				setState(253);
				modifier();
				}
			}

			setState(256);
			match(BOOLEAN);
			setState(257);
			match(IDENTIFIER);
			setState(261);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(258);
					parallel_declaration();
					}
					} 
				}
				setState(263);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			setState(264);
			match(EQ);
			setState(265);
			boolean_literal();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parallel_declarationContext extends ParserRuleContext {
		public TerminalNode EQ() { return getToken(OurJavaParser.EQ, 0); }
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public Parallel_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallel_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterParallel_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitParallel_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitParallel_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parallel_declarationContext parallel_declaration() throws RecognitionException {
		Parallel_declarationContext _localctx = new Parallel_declarationContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_parallel_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			match(EQ);
			setState(268);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(OurJavaParser.LPAREN, 0); }
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(OurJavaParser.RPAREN, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			match(LPAREN);
			setState(271);
			expression_body(0);
			setState(272);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public Integer_literalContext integer_literal() {
			return getRuleContext(Integer_literalContext.class,0);
		}
		public Boolean_literalContext boolean_literal() {
			return getRuleContext(Boolean_literalContext.class,0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_literal);
		try {
			setState(276);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(274);
				integer_literal();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(275);
				boolean_literal();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Boolean_literalContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(OurJavaParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(OurJavaParser.FALSE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public Function_call_statementContext function_call_statement() {
			return getRuleContext(Function_call_statementContext.class,0);
		}
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public Boolean_literalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolean_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterBoolean_literal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitBoolean_literal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitBoolean_literal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Boolean_literalContext boolean_literal() throws RecognitionException {
		Boolean_literalContext _localctx = new Boolean_literalContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_boolean_literal);
		try {
			setState(283);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(278);
				match(TRUE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(279);
				match(FALSE);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(280);
				match(IDENTIFIER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(281);
				function_call_statement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(282);
				expression_body(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Integer_literalContext extends ParserRuleContext {
		public TerminalNode DecimalNumeral() { return getToken(OurJavaParser.DecimalNumeral, 0); }
		public Decimal_symbolContext decimal_symbol() {
			return getRuleContext(Decimal_symbolContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public Function_call_statementContext function_call_statement() {
			return getRuleContext(Function_call_statementContext.class,0);
		}
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public Integer_literalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterInteger_literal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitInteger_literal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitInteger_literal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Integer_literalContext integer_literal() throws RecognitionException {
		Integer_literalContext _localctx = new Integer_literalContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_integer_literal);
		int _la;
		try {
			setState(292);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(286);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(285);
					decimal_symbol();
					}
				}

				setState(288);
				match(DecimalNumeral);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(289);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(290);
				function_call_statement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(291);
				expression_body(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_typeContext extends ParserRuleContext {
		public TerminalNode TO() { return getToken(OurJavaParser.TO, 0); }
		public TerminalNode DOWNTO() { return getToken(OurJavaParser.DOWNTO, 0); }
		public For_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFor_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFor_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFor_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_typeContext for_type() throws RecognitionException {
		For_typeContext _localctx = new For_typeContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_for_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(294);
			_la = _input.LA(1);
			if ( !(_la==DOWNTO || _la==TO) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_initContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(OurJavaParser.INT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public TerminalNode EQ() { return getToken(OurJavaParser.EQ, 0); }
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public For_initContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFor_init(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFor_init(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFor_init(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_initContext for_init() throws RecognitionException {
		For_initContext _localctx = new For_initContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_for_init);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(296);
			match(INT);
			setState(297);
			match(IDENTIFIER);
			setState(298);
			match(EQ);
			setState(299);
			expression_body(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_statementContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(OurJavaParser.LPAREN, 0); }
		public For_initContext for_init() {
			return getRuleContext(For_initContext.class,0);
		}
		public For_typeContext for_type() {
			return getRuleContext(For_typeContext.class,0);
		}
		public Integer_literalContext integer_literal() {
			return getRuleContext(Integer_literalContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(OurJavaParser.RPAREN, 0); }
		public For_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFor_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFor_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFor_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_statementContext for_statement() throws RecognitionException {
		For_statementContext _localctx = new For_statementContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_for_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(301);
			match(LPAREN);
			setState(302);
			for_init();
			setState(303);
			for_type();
			setState(304);
			integer_literal();
			setState(305);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_blockContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(OurJavaParser.CASE, 0); }
		public TerminalNode DecimalNumeral() { return getToken(OurJavaParser.DecimalNumeral, 0); }
		public TerminalNode COLON() { return getToken(OurJavaParser.COLON, 0); }
		public Switch_bodyContext switch_body() {
			return getRuleContext(Switch_bodyContext.class,0);
		}
		public Switch_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterSwitch_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitSwitch_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitSwitch_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_blockContext switch_block() throws RecognitionException {
		Switch_blockContext _localctx = new Switch_blockContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_switch_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(307);
			match(CASE);
			setState(308);
			match(DecimalNumeral);
			setState(309);
			match(COLON);
			setState(310);
			switch_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_declarationContext extends ParserRuleContext {
		public Function_headerContext function_header() {
			return getRuleContext(Function_headerContext.class,0);
		}
		public Function_bodyContext function_body() {
			return getRuleContext(Function_bodyContext.class,0);
		}
		public Function_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunction_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunction_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunction_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_declarationContext function_declaration() throws RecognitionException {
		Function_declarationContext _localctx = new Function_declarationContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_function_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(312);
			function_header();
			setState(313);
			function_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_headerContext extends ParserRuleContext {
		public Function_typeContext function_type() {
			return getRuleContext(Function_typeContext.class,0);
		}
		public Function_declaratorContext function_declarator() {
			return getRuleContext(Function_declaratorContext.class,0);
		}
		public Function_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunction_header(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunction_header(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunction_header(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_headerContext function_header() throws RecognitionException {
		Function_headerContext _localctx = new Function_headerContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_function_header);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(315);
			function_type();
			setState(316);
			function_declarator();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_declaratorContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public TerminalNode LPAREN() { return getToken(OurJavaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(OurJavaParser.RPAREN, 0); }
		public List<Formal_parameterContext> formal_parameter() {
			return getRuleContexts(Formal_parameterContext.class);
		}
		public Formal_parameterContext formal_parameter(int i) {
			return getRuleContext(Formal_parameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(OurJavaParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(OurJavaParser.COMMA, i);
		}
		public Function_declaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunction_declarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunction_declarator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunction_declarator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_declaratorContext function_declarator() throws RecognitionException {
		Function_declaratorContext _localctx = new Function_declaratorContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_function_declarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(318);
			match(IDENTIFIER);
			setState(319);
			match(LPAREN);
			setState(328);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BOOLEAN || _la==INT) {
				{
				setState(320);
				formal_parameter();
				setState(325);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(321);
					match(COMMA);
					setState(322);
					formal_parameter();
					}
					}
					setState(327);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(330);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_typeContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(OurJavaParser.INT, 0); }
		public TerminalNode BOOLEAN() { return getToken(OurJavaParser.BOOLEAN, 0); }
		public TerminalNode VOID() { return getToken(OurJavaParser.VOID, 0); }
		public Function_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunction_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunction_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunction_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_typeContext function_type() throws RecognitionException {
		Function_typeContext _localctx = new Function_typeContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_function_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(332);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << VOID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_bodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(OurJavaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(OurJavaParser.RBRACE, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public Function_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunction_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunction_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunction_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_bodyContext function_body() throws RecognitionException {
		Function_bodyContext _localctx = new Function_bodyContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_function_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(334);
			match(LBRACE);
			setState(336);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << INT) | (1L << FINAL) | (1L << IF) | (1L << SWITCH) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << REPEAT) | (1L << RETURN) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(335);
				block_body();
				}
			}

			setState(338);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Formal_parameterContext extends ParserRuleContext {
		public VarTypesContext varTypes() {
			return getRuleContext(VarTypesContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public Formal_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formal_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFormal_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFormal_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFormal_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Formal_parameterContext formal_parameter() throws RecognitionException {
		Formal_parameterContext _localctx = new Formal_parameterContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_formal_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(340);
			varTypes();
			setState(341);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_call_statementContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(OurJavaParser.IDENTIFIER, 0); }
		public TerminalNode LPAREN() { return getToken(OurJavaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(OurJavaParser.RPAREN, 0); }
		public List<Argument_listContext> argument_list() {
			return getRuleContexts(Argument_listContext.class);
		}
		public Argument_listContext argument_list(int i) {
			return getRuleContext(Argument_listContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(OurJavaParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(OurJavaParser.COMMA, i);
		}
		public Function_call_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterFunction_call_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitFunction_call_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitFunction_call_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_call_statementContext function_call_statement() throws RecognitionException {
		Function_call_statementContext _localctx = new Function_call_statementContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_function_call_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(343);
			match(IDENTIFIER);
			setState(344);
			match(LPAREN);
			setState(353);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TRUE) | (1L << FALSE) | (1L << PLUS) | (1L << MINUS) | (1L << NEGATION) | (1L << LPAREN) | (1L << IDENTIFIER) | (1L << DecimalNumeral))) != 0)) {
				{
				setState(345);
				argument_list();
				setState(350);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(346);
					match(COMMA);
					setState(347);
					argument_list();
					}
					}
					setState(352);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(355);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Argument_listContext extends ParserRuleContext {
		public Expression_bodyContext expression_body() {
			return getRuleContext(Expression_bodyContext.class,0);
		}
		public Argument_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).enterArgument_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof OurJavaListener ) ((OurJavaListener)listener).exitArgument_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof OurJavaVisitor ) return ((OurJavaVisitor<? extends T>)visitor).visitArgument_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Argument_listContext argument_list() throws RecognitionException {
		Argument_listContext _localctx = new Argument_listContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_argument_list);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(357);
			expression_body(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 17:
			return expression_body_sempred((Expression_bodyContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_body_sempred(Expression_bodyContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\60\u016a\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\5\2Q\n\2\3\3\3\3\3"+
		"\4\5\4V\n\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\t\3\t\5\te\n"+
		"\t\3\t\3\t\3\n\3\n\5\nk\n\n\3\n\3\n\3\13\6\13p\n\13\r\13\16\13q\3\f\3"+
		"\f\5\fv\n\f\3\r\6\ry\n\r\r\r\16\rz\3\16\7\16~\n\16\f\16\16\16\u0081\13"+
		"\16\3\17\3\17\3\17\5\17\u0086\n\17\3\20\3\20\3\20\3\20\3\20\5\20\u008d"+
		"\n\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\7\20\u00aa\n\20\f\20\16\20\u00ad\13\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\5\20\u00b9\n\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22"+
		"\3\22\5\22\u00c3\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\5\23\u00d1\n\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u00e5\n\23\f\23\16"+
		"\23\u00e8\13\23\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00f0\n\24\3\25\5\25"+
		"\u00f3\n\25\3\25\3\25\3\25\7\25\u00f8\n\25\f\25\16\25\u00fb\13\25\3\25"+
		"\3\25\3\25\3\26\5\26\u0101\n\26\3\26\3\26\3\26\7\26\u0106\n\26\f\26\16"+
		"\26\u0109\13\26\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\31"+
		"\3\31\5\31\u0117\n\31\3\32\3\32\3\32\3\32\3\32\5\32\u011e\n\32\3\33\5"+
		"\33\u0121\n\33\3\33\3\33\3\33\3\33\5\33\u0127\n\33\3\34\3\34\3\35\3\35"+
		"\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37"+
		"\3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3\"\3\"\7\"\u0146\n\"\f\"\16\"\u0149\13"+
		"\"\5\"\u014b\n\"\3\"\3\"\3#\3#\3$\3$\5$\u0153\n$\3$\3$\3%\3%\3%\3&\3&"+
		"\3&\3&\3&\7&\u015f\n&\f&\16&\u0162\13&\5&\u0164\n&\3&\3&\3\'\3\'\3\'\2"+
		"\3$(\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@"+
		"BDFHJL\2\n\3\2\5\6\3\2\3\4\3\2\36\37\3\2 !\3\2\30\35\3\2\26\27\3\2\16"+
		"\17\4\2\3\4\24\24\2\u0176\2P\3\2\2\2\4R\3\2\2\2\6U\3\2\2\2\bY\3\2\2\2"+
		"\n[\3\2\2\2\f]\3\2\2\2\16_\3\2\2\2\20b\3\2\2\2\22h\3\2\2\2\24o\3\2\2\2"+
		"\26u\3\2\2\2\30x\3\2\2\2\32\177\3\2\2\2\34\u0085\3\2\2\2\36\u00b8\3\2"+
		"\2\2 \u00ba\3\2\2\2\"\u00c2\3\2\2\2$\u00d0\3\2\2\2&\u00ef\3\2\2\2(\u00f2"+
		"\3\2\2\2*\u0100\3\2\2\2,\u010d\3\2\2\2.\u0110\3\2\2\2\60\u0116\3\2\2\2"+
		"\62\u011d\3\2\2\2\64\u0126\3\2\2\2\66\u0128\3\2\2\28\u012a\3\2\2\2:\u012f"+
		"\3\2\2\2<\u0135\3\2\2\2>\u013a\3\2\2\2@\u013d\3\2\2\2B\u0140\3\2\2\2D"+
		"\u014e\3\2\2\2F\u0150\3\2\2\2H\u0156\3\2\2\2J\u0159\3\2\2\2L\u0167\3\2"+
		"\2\2NQ\5\4\3\2OQ\5\6\4\2PN\3\2\2\2PO\3\2\2\2Q\3\3\2\2\2RS\t\2\2\2S\5\3"+
		"\2\2\2TV\5\n\6\2UT\3\2\2\2UV\3\2\2\2VW\3\2\2\2WX\7-\2\2X\7\3\2\2\2YZ\t"+
		"\3\2\2Z\t\3\2\2\2[\\\t\4\2\2\\\13\3\2\2\2]^\7\7\2\2^\r\3\2\2\2_`\5\20"+
		"\t\2`a\7\2\2\3a\17\3\2\2\2bd\7(\2\2ce\5\24\13\2dc\3\2\2\2de\3\2\2\2ef"+
		"\3\2\2\2fg\7)\2\2g\21\3\2\2\2hj\7(\2\2ik\5\30\r\2ji\3\2\2\2jk\3\2\2\2"+
		"kl\3\2\2\2lm\7)\2\2m\23\3\2\2\2np\5\26\f\2on\3\2\2\2pq\3\2\2\2qo\3\2\2"+
		"\2qr\3\2\2\2r\25\3\2\2\2sv\5> \2tv\5\36\20\2us\3\2\2\2ut\3\2\2\2v\27\3"+
		"\2\2\2wy\5\36\20\2xw\3\2\2\2yz\3\2\2\2zx\3\2\2\2z{\3\2\2\2{\31\3\2\2\2"+
		"|~\5\34\17\2}|\3\2\2\2~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080\3\2\2\2\u0080"+
		"\33\3\2\2\2\u0081\177\3\2\2\2\u0082\u0086\5\36\20\2\u0083\u0084\7\f\2"+
		"\2\u0084\u0086\7*\2\2\u0085\u0082\3\2\2\2\u0085\u0083\3\2\2\2\u0086\35"+
		"\3\2\2\2\u0087\u0088\7\b\2\2\u0088\u0089\5.\30\2\u0089\u008c\5\22\n\2"+
		"\u008a\u008b\7\t\2\2\u008b\u008d\5\22\n\2\u008c\u008a\3\2\2\2\u008c\u008d"+
		"\3\2\2\2\u008d\u00b9\3\2\2\2\u008e\u008f\7\20\2\2\u008f\u0090\5.\30\2"+
		"\u0090\u0091\5\22\n\2\u0091\u00b9\3\2\2\2\u0092\u0093\7\21\2\2\u0093\u0094"+
		"\5\22\n\2\u0094\u0095\7\20\2\2\u0095\u0096\5.\30\2\u0096\u00b9\3\2\2\2"+
		"\u0097\u0098\7\20\2\2\u0098\u0099\5.\30\2\u0099\u009a\7\21\2\2\u009a\u009b"+
		"\5\22\n\2\u009b\u00b9\3\2\2\2\u009c\u009d\7\22\2\2\u009d\u009e\5\22\n"+
		"\2\u009e\u009f\7\23\2\2\u009f\u00a0\5.\30\2\u00a0\u00b9\3\2\2\2\u00a1"+
		"\u00a2\7\r\2\2\u00a2\u00a3\5:\36\2\u00a3\u00a4\5\22\n\2\u00a4\u00b9\3"+
		"\2\2\2\u00a5\u00a6\7\n\2\2\u00a6\u00a7\5.\30\2\u00a7\u00ab\7(\2\2\u00a8"+
		"\u00aa\5<\37\2\u00a9\u00a8\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab\u00a9\3\2"+
		"\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00ae\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ae"+
		"\u00af\7)\2\2\u00af\u00b9\3\2\2\2\u00b0\u00b1\5J&\2\u00b1\u00b2\7*\2\2"+
		"\u00b2\u00b9\3\2\2\2\u00b3\u00b9\5 \21\2\u00b4\u00b9\5&\24\2\u00b5\u00b6"+
		"\5\"\22\2\u00b6\u00b7\7*\2\2\u00b7\u00b9\3\2\2\2\u00b8\u0087\3\2\2\2\u00b8"+
		"\u008e\3\2\2\2\u00b8\u0092\3\2\2\2\u00b8\u0097\3\2\2\2\u00b8\u009c\3\2"+
		"\2\2\u00b8\u00a1\3\2\2\2\u00b8\u00a5\3\2\2\2\u00b8\u00b0\3\2\2\2\u00b8"+
		"\u00b3\3\2\2\2\u00b8\u00b4\3\2\2\2\u00b8\u00b5\3\2\2\2\u00b9\37\3\2\2"+
		"\2\u00ba\u00bb\7,\2\2\u00bb\u00bc\7\"\2\2\u00bc\u00bd\5$\23\2\u00bd\u00be"+
		"\7*\2\2\u00be!\3\2\2\2\u00bf\u00c0\7\25\2\2\u00c0\u00c3\5$\23\2\u00c1"+
		"\u00c3\7\25\2\2\u00c2\u00bf\3\2\2\2\u00c2\u00c1\3\2\2\2\u00c3#\3\2\2\2"+
		"\u00c4\u00c5\b\23\1\2\u00c5\u00d1\5\2\2\2\u00c6\u00d1\7,\2\2\u00c7\u00d1"+
		"\5J&\2\u00c8\u00c9\7&\2\2\u00c9\u00ca\5$\23\2\u00ca\u00cb\7\'\2\2\u00cb"+
		"\u00d1\3\2\2\2\u00cc\u00cd\7%\2\2\u00cd\u00d1\5$\23\5\u00ce\u00cf\t\4"+
		"\2\2\u00cf\u00d1\5$\23\4\u00d0\u00c4\3\2\2\2\u00d0\u00c6\3\2\2\2\u00d0"+
		"\u00c7\3\2\2\2\u00d0\u00c8\3\2\2\2\u00d0\u00cc\3\2\2\2\u00d0\u00ce\3\2"+
		"\2\2\u00d1\u00e6\3\2\2\2\u00d2\u00d3\f\n\2\2\u00d3\u00d4\t\5\2\2\u00d4"+
		"\u00e5\5$\23\13\u00d5\u00d6\f\t\2\2\u00d6\u00d7\t\4\2\2\u00d7\u00e5\5"+
		"$\23\n\u00d8\u00d9\f\b\2\2\u00d9\u00da\t\6\2\2\u00da\u00e5\5$\23\t\u00db"+
		"\u00dc\f\7\2\2\u00dc\u00dd\t\7\2\2\u00dd\u00e5\5$\23\b\u00de\u00df\f\3"+
		"\2\2\u00df\u00e0\7#\2\2\u00e0\u00e1\5$\23\2\u00e1\u00e2\7$\2\2\u00e2\u00e3"+
		"\5$\23\4\u00e3\u00e5\3\2\2\2\u00e4\u00d2\3\2\2\2\u00e4\u00d5\3\2\2\2\u00e4"+
		"\u00d8\3\2\2\2\u00e4\u00db\3\2\2\2\u00e4\u00de\3\2\2\2\u00e5\u00e8\3\2"+
		"\2\2\u00e6\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7%\3\2\2\2\u00e8\u00e6"+
		"\3\2\2\2\u00e9\u00ea\5(\25\2\u00ea\u00eb\7*\2\2\u00eb\u00f0\3\2\2\2\u00ec"+
		"\u00ed\5*\26\2\u00ed\u00ee\7*\2\2\u00ee\u00f0\3\2\2\2\u00ef\u00e9\3\2"+
		"\2\2\u00ef\u00ec\3\2\2\2\u00f0\'\3\2\2\2\u00f1\u00f3\5\f\7\2\u00f2\u00f1"+
		"\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\7\4\2\2\u00f5"+
		"\u00f9\7,\2\2\u00f6\u00f8\5,\27\2\u00f7\u00f6\3\2\2\2\u00f8\u00fb\3\2"+
		"\2\2\u00f9\u00f7\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa\u00fc\3\2\2\2\u00fb"+
		"\u00f9\3\2\2\2\u00fc\u00fd\7\"\2\2\u00fd\u00fe\5\64\33\2\u00fe)\3\2\2"+
		"\2\u00ff\u0101\5\f\7\2\u0100\u00ff\3\2\2\2\u0100\u0101\3\2\2\2\u0101\u0102"+
		"\3\2\2\2\u0102\u0103\7\3\2\2\u0103\u0107\7,\2\2\u0104\u0106\5,\27\2\u0105"+
		"\u0104\3\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2"+
		"\2\2\u0108\u010a\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u010b\7\"\2\2\u010b"+
		"\u010c\5\62\32\2\u010c+\3\2\2\2\u010d\u010e\7\"\2\2\u010e\u010f\7,\2\2"+
		"\u010f-\3\2\2\2\u0110\u0111\7&\2\2\u0111\u0112\5$\23\2\u0112\u0113\7\'"+
		"\2\2\u0113/\3\2\2\2\u0114\u0117\5\64\33\2\u0115\u0117\5\62\32\2\u0116"+
		"\u0114\3\2\2\2\u0116\u0115\3\2\2\2\u0117\61\3\2\2\2\u0118\u011e\7\5\2"+
		"\2\u0119\u011e\7\6\2\2\u011a\u011e\7,\2\2\u011b\u011e\5J&\2\u011c\u011e"+
		"\5$\23\2\u011d\u0118\3\2\2\2\u011d\u0119\3\2\2\2\u011d\u011a\3\2\2\2\u011d"+
		"\u011b\3\2\2\2\u011d\u011c\3\2\2\2\u011e\63\3\2\2\2\u011f\u0121\5\n\6"+
		"\2\u0120\u011f\3\2\2\2\u0120\u0121\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0127"+
		"\7-\2\2\u0123\u0127\7,\2\2\u0124\u0127\5J&\2\u0125\u0127\5$\23\2\u0126"+
		"\u0120\3\2\2\2\u0126\u0123\3\2\2\2\u0126\u0124\3\2\2\2\u0126\u0125\3\2"+
		"\2\2\u0127\65\3\2\2\2\u0128\u0129\t\b\2\2\u0129\67\3\2\2\2\u012a\u012b"+
		"\7\4\2\2\u012b\u012c\7,\2\2\u012c\u012d\7\"\2\2\u012d\u012e\5$\23\2\u012e"+
		"9\3\2\2\2\u012f\u0130\7&\2\2\u0130\u0131\58\35\2\u0131\u0132\5\66\34\2"+
		"\u0132\u0133\5\64\33\2\u0133\u0134\7\'\2\2\u0134;\3\2\2\2\u0135\u0136"+
		"\7\13\2\2\u0136\u0137\7-\2\2\u0137\u0138\7$\2\2\u0138\u0139\5\32\16\2"+
		"\u0139=\3\2\2\2\u013a\u013b\5@!\2\u013b\u013c\5F$\2\u013c?\3\2\2\2\u013d"+
		"\u013e\5D#\2\u013e\u013f\5B\"\2\u013fA\3\2\2\2\u0140\u0141\7,\2\2\u0141"+
		"\u014a\7&\2\2\u0142\u0147\5H%\2\u0143\u0144\7+\2\2\u0144\u0146\5H%\2\u0145"+
		"\u0143\3\2\2\2\u0146\u0149\3\2\2\2\u0147\u0145\3\2\2\2\u0147\u0148\3\2"+
		"\2\2\u0148\u014b\3\2\2\2\u0149\u0147\3\2\2\2\u014a\u0142\3\2\2\2\u014a"+
		"\u014b\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u014d\7\'\2\2\u014dC\3\2\2\2"+
		"\u014e\u014f\t\t\2\2\u014fE\3\2\2\2\u0150\u0152\7(\2\2\u0151\u0153\5\30"+
		"\r\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0154\3\2\2\2\u0154"+
		"\u0155\7)\2\2\u0155G\3\2\2\2\u0156\u0157\5\b\5\2\u0157\u0158\7,\2\2\u0158"+
		"I\3\2\2\2\u0159\u015a\7,\2\2\u015a\u0163\7&\2\2\u015b\u0160\5L\'\2\u015c"+
		"\u015d\7+\2\2\u015d\u015f\5L\'\2\u015e\u015c\3\2\2\2\u015f\u0162\3\2\2"+
		"\2\u0160\u015e\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0164\3\2\2\2\u0162\u0160"+
		"\3\2\2\2\u0163\u015b\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0165\3\2\2\2\u0165"+
		"\u0166\7\'\2\2\u0166K\3\2\2\2\u0167\u0168\5$\23\2\u0168M\3\2\2\2 PUdj"+
		"quz\177\u0085\u008c\u00ab\u00b8\u00c2\u00d0\u00e4\u00e6\u00ef\u00f2\u00f9"+
		"\u0100\u0107\u0116\u011d\u0120\u0126\u0147\u014a\u0152\u0160\u0163";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}