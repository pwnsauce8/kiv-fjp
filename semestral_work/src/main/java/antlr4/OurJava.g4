/*
 * https://github.com/antlr/antlr4/blob/master/tool-testsuite/test/org/antlr/v4/test/tool/Java.g4
*/

grammar OurJava;


/* ------ Keywords ------  */
/* Types */
BOOLEAN : 'boolean' ;
INT   : 'int' ;

/* Boolean Literals */
TRUE  : 'true' ;
FALSE : 'false' ;

/* Modifiers */
FINAL : 'final';

/* If-else */
IF    : 'if' ;
ELSE  : 'else' ;

/* Switch */
SWITCH  : 'switch' ;
CASE  : 'case' ;
BREAK : 'break';

/* For */
FOR   : 'for' ;
DOWNTO   : 'downto' ;
TO   : 'to' ;

/* While and repeat-until */
WHILE : 'while' ;
DO    : 'do' ;
REPEAT  : 'repeat' ;
UNTIL : 'until' ;

/* Methods */
VOID  : 'void' ;
RETURN : 'return' ;


/* ------ Assignment operators ------ */
OR:         '||' ;
AND:        '&&' ;
SAME:       '==' ;
NOT_EQ :    '!=' ;
LT:         '<' ;
GT:         '>' ;
LE:         '<=' ;
GE:         '>=' ;
PLUS:       '+' ;
MINUS:      '-' ;
MULT:       '*' ;
DIV:        '/' ;
EQ:         '=' ;
QUESTION :  '?' ;
COLON :     ':' ;
NEGATION :  '!';
/*XUPPER :    'X';
XLOW :      'x';*/


/* ------ Separators ------ */
LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
SEMI:   ';';
COMMA:  ',';

IDENTIFIER: Letter Letter_or_digit*;


/* ------ Literals ------ */
fragment ZERO : '0' ;

DecimalNumeral
	:	ZERO
	|	NonZeroDigit (Digits? | Underscores Digits)
	;

fragment Digits
	:	Digit (DigitsAndUnderscores? Digit)?
	;

fragment Digit
	:	ZERO
	|	NonZeroDigit
	;

fragment NonZeroDigit
	:	[1-9]
	;

fragment DigitsAndUnderscores
	:	DigitOrUnderscore+
	;

fragment DigitOrUnderscore
	:	Digit
	|	'_'
	;

fragment Underscores
	:	'_'+
	;

fragment HexDigits
	: HexDigit (HexDigitsAndUnderscores? HexDigit)?
	;

fragment HexDigit
	:	[0-9a-fA-F]
	;

fragment HexDigitsAndUnderscores
	:	HexDigitOrUnderscore+
	;

fragment HexDigitOrUnderscore
	:	HexDigit
	|	'_'
	;

fragment Letter
    : [a-zA-Z$_]
    ;

fragment Letter_or_digit
    : [a-zA-Z0-9$_]
    ;

WHITESPACE
    : [ \t\r\n\u000C]+ -> skip
    ;

COMMENT
    : '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    : '//' ~[\r\n]* -> skip
    ;

/* -------------- Grammar -------------- */

values
    : boolean_values
    | decimal_values
    ;

boolean_values
    : TRUE
    | FALSE
    ;

decimal_values
    : decimal_symbol? DecimalNumeral
    ;

varTypes
    : INT
    | BOOLEAN
    ;

decimal_symbol
    : PLUS
    | MINUS
    ;

modifier
    : FINAL
    ;

/* ============= Program ============= */
program
    : block EOF
    ;

block
    : LBRACE block_statement? RBRACE
    ;

body
    : LBRACE block_body? RBRACE
    ;

block_statement
    : (statements)+
    ;
    
statements
	: function_declaration
    | statement
    ;

block_body
    : (statement)+
    ;
    
switch_body
	: (switch_body_statement)*
	;
	
switch_body_statement
	: statement
	| BREAK SEMI
	;

/* ============= Identifiers ============= */
/*identifier
	:	Letter Letter_or_digit*
	;*/

/* ============= Statements ============= */
statement
    : IF expression body (ELSE body)?               #ifelseStatement
    | WHILE expression body                         #whileStatement
    | DO body WHILE expression                      #dowhileStatement
    | WHILE expression DO body                      #whiledoStatement
    | REPEAT body UNTIL expression                  #repeatuntilStatement
    | FOR for_statement body                        #forStatement
    | SWITCH expression LBRACE switch_block* RBRACE #switchStatement
    | function_call_statement SEMI                      #functioncallStatement
    | variable_assigment                            #varassigmentStatement
    | variable_declaration                          #vardeclarationStatement
    | return_statement SEMI                         #returnStatement
    ;

variable_assigment
    : IDENTIFIER EQ expression_body SEMI
    ;

return_statement
    : RETURN expression_body
    | RETURN
    ;

/* ============= Expression ============= */
expression_body
    : values                                                                    #valuesExpression
    | IDENTIFIER                                                                #identifierExpression
    | function_call_statement                                                   #funccallExpression
    | expression_body op=(MULT | DIV) expression_body                           #multanddivExpression
    | expression_body op=(PLUS | MINUS) expression_body                         #plusandminusExpression
    | expression_body op=(GT | GE | LT | LE | SAME | NOT_EQ) expression_body    #relationalExpression
    | expression_body op=(AND | OR) expression_body                             #logicalExpression
    | LPAREN expression_body RPAREN                                             #expbodyExpression
    | NEGATION expression_body                                                  #negationExpression
    | (MINUS|PLUS) expression_body												#minusPlusExpression
    | expression_body QUESTION expression_body COLON expression_body			#ternaryExpression
    ;

/* ============= Variable declaration ============= */
variable_declaration
    :  decimal_variable SEMI
    |  bool_variable SEMI
    ;

decimal_variable
    : (modifier)? INT IDENTIFIER (parallel_declaration)* EQ integer_literal
    ;

bool_variable
    : (modifier)? BOOLEAN IDENTIFIER (parallel_declaration)* EQ boolean_literal
    ;

parallel_declaration
    : EQ IDENTIFIER
    ;

/* ============= Expression ============= */

expression
    : LPAREN expression_body RPAREN
    ;

/* ============= LITERALS ============= */
literal
    : integer_literal
    | boolean_literal
    ;

/* Boolean literal */
boolean_literal
    : TRUE
    | FALSE
    | IDENTIFIER
    | function_call_statement
    | expression_body
    ;

/* Decimal integer literal */
integer_literal
    : decimal_symbol? DecimalNumeral
    | IDENTIFIER
    | function_call_statement
    | expression_body
    ;


/* ============= FOR ============= */
for_type
  : TO
  | DOWNTO
  ;

for_init
  : INT IDENTIFIER EQ expression_body
  ;

for_statement
  : LPAREN for_init for_type integer_literal RPAREN
  ;

/* ============= SWITCH ============= */
switch_block
  : CASE DecimalNumeral COLON switch_body
  ;

/* ============= FUNCTION ============= */
function_declaration
  : function_header function_body
  ;

function_header
  : function_type function_declarator
  ;

function_declarator
  : IDENTIFIER LPAREN (formal_parameter (COMMA formal_parameter)*)? RPAREN
  ;

function_type
  : INT
  | BOOLEAN
  | VOID
  ;

function_body
  : LBRACE (block_body)? RBRACE
  ;

formal_parameter
  : varTypes IDENTIFIER
  ;

function_call_statement
  : IDENTIFIER LPAREN (argument_list (COMMA argument_list)*)? RPAREN
  ;

argument_list
  : expression_body
  ;