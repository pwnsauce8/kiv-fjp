// Generated from OurJava.g4 by ANTLR 4.9
package main.java.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link OurJavaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface OurJavaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code values}
	 * labeled alternative in {@link OurJavaParser#statementstatementstatementstatementstatementstatementstatementstatementstatementstatementstatementexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValues(OurJavaParser.ValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#boolean_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolean_values(OurJavaParser.Boolean_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#decimal_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimal_values(OurJavaParser.Decimal_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#varTypes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarTypes(OurJavaParser.VarTypesContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#decimal_symbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimal_symbol(OurJavaParser.Decimal_symbolContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#modifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModifier(OurJavaParser.ModifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(OurJavaParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(OurJavaParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(OurJavaParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#block_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_statement(OurJavaParser.Block_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatements(OurJavaParser.StatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#block_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_body(OurJavaParser.Block_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#switch_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_body(OurJavaParser.Switch_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#switch_body_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_body_statement(OurJavaParser.Switch_body_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifelseStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfelseStatement(OurJavaParser.IfelseStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(OurJavaParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dowhileStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowhileStatement(OurJavaParser.DowhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whiledoStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhiledoStatement(OurJavaParser.WhiledoStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repeatuntilStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeatuntilStatement(OurJavaParser.RepeatuntilStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement(OurJavaParser.ForStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code switchStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchStatement(OurJavaParser.SwitchStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functioncallStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctioncallStatement(OurJavaParser.FunctioncallStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varassigmentStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarassigmentStatement(OurJavaParser.VarassigmentStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code vardeclarationStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVardeclarationStatement(OurJavaParser.VardeclarationStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement(OurJavaParser.ReturnStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#variable_assigment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_assigment(OurJavaParser.Variable_assigmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#return_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_statement(OurJavaParser.Return_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegationExpression(OurJavaParser.NegationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expbodyExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpbodyExpression(OurJavaParser.ExpbodyExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funccallExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunccallExpression(OurJavaParser.FunccallExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpression(OurJavaParser.RelationalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valuesExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuesExpression(OurJavaParser.ValuesExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierExpression(OurJavaParser.IdentifierExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plusandminusExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusandminusExpression(OurJavaParser.PlusandminusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code minusPlusExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusPlusExpression(OurJavaParser.MinusPlusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logicalExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalExpression(OurJavaParser.LogicalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernaryExpression(OurJavaParser.TernaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multanddivExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultanddivExpression(OurJavaParser.MultanddivExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#variable_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_declaration(OurJavaParser.Variable_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#decimal_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimal_variable(OurJavaParser.Decimal_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#bool_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool_variable(OurJavaParser.Bool_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#parallel_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParallel_declaration(OurJavaParser.Parallel_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(OurJavaParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(OurJavaParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#boolean_literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolean_literal(OurJavaParser.Boolean_literalContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#integer_literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger_literal(OurJavaParser.Integer_literalContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#for_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_type(OurJavaParser.For_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#for_init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_init(OurJavaParser.For_initContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#for_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_statement(OurJavaParser.For_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#switch_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_block(OurJavaParser.Switch_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#function_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_declaration(OurJavaParser.Function_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#function_header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_header(OurJavaParser.Function_headerContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#function_declarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_declarator(OurJavaParser.Function_declaratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#function_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_type(OurJavaParser.Function_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#function_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_body(OurJavaParser.Function_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#formal_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormal_parameter(OurJavaParser.Formal_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#function_call_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_call_statement(OurJavaParser.Function_call_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link OurJavaParser#argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument_list(OurJavaParser.Argument_listContext ctx);
}