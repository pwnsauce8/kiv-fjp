// Generated from OurJava.g4 by ANTLR 4.9
package main.java.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OurJavaParser}.
 */
public interface OurJavaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code values}
	 * labeled alternative in {@link OurJavaParser#statementstatementstatementstatementstatementstatementstatementstatementstatementstatementstatementexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_body}.
	 * @param ctx the parse tree
	 */
	void enterValues(OurJavaParser.ValuesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code values}
	 * labeled alternative in {@link OurJavaParser#statementstatementstatementstatementstatementstatementstatementstatementstatementstatementstatementexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_bodyexpression_body}.
	 * @param ctx the parse tree
	 */
	void exitValues(OurJavaParser.ValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#boolean_values}.
	 * @param ctx the parse tree
	 */
	void enterBoolean_values(OurJavaParser.Boolean_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#boolean_values}.
	 * @param ctx the parse tree
	 */
	void exitBoolean_values(OurJavaParser.Boolean_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#decimal_values}.
	 * @param ctx the parse tree
	 */
	void enterDecimal_values(OurJavaParser.Decimal_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#decimal_values}.
	 * @param ctx the parse tree
	 */
	void exitDecimal_values(OurJavaParser.Decimal_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#varTypes}.
	 * @param ctx the parse tree
	 */
	void enterVarTypes(OurJavaParser.VarTypesContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#varTypes}.
	 * @param ctx the parse tree
	 */
	void exitVarTypes(OurJavaParser.VarTypesContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#decimal_symbol}.
	 * @param ctx the parse tree
	 */
	void enterDecimal_symbol(OurJavaParser.Decimal_symbolContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#decimal_symbol}.
	 * @param ctx the parse tree
	 */
	void exitDecimal_symbol(OurJavaParser.Decimal_symbolContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#modifier}.
	 * @param ctx the parse tree
	 */
	void enterModifier(OurJavaParser.ModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#modifier}.
	 * @param ctx the parse tree
	 */
	void exitModifier(OurJavaParser.ModifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(OurJavaParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(OurJavaParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(OurJavaParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(OurJavaParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(OurJavaParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(OurJavaParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#block_statement}.
	 * @param ctx the parse tree
	 */
	void enterBlock_statement(OurJavaParser.Block_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#block_statement}.
	 * @param ctx the parse tree
	 */
	void exitBlock_statement(OurJavaParser.Block_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(OurJavaParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(OurJavaParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#block_body}.
	 * @param ctx the parse tree
	 */
	void enterBlock_body(OurJavaParser.Block_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#block_body}.
	 * @param ctx the parse tree
	 */
	void exitBlock_body(OurJavaParser.Block_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#switch_body}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_body(OurJavaParser.Switch_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#switch_body}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_body(OurJavaParser.Switch_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#switch_body_statement}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_body_statement(OurJavaParser.Switch_body_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#switch_body_statement}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_body_statement(OurJavaParser.Switch_body_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifelseStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIfelseStatement(OurJavaParser.IfelseStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifelseStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIfelseStatement(OurJavaParser.IfelseStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(OurJavaParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(OurJavaParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dowhileStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterDowhileStatement(OurJavaParser.DowhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dowhileStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitDowhileStatement(OurJavaParser.DowhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whiledoStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhiledoStatement(OurJavaParser.WhiledoStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whiledoStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhiledoStatement(OurJavaParser.WhiledoStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repeatuntilStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterRepeatuntilStatement(OurJavaParser.RepeatuntilStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repeatuntilStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitRepeatuntilStatement(OurJavaParser.RepeatuntilStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(OurJavaParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(OurJavaParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code switchStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterSwitchStatement(OurJavaParser.SwitchStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code switchStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitSwitchStatement(OurJavaParser.SwitchStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functioncallStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFunctioncallStatement(OurJavaParser.FunctioncallStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functioncallStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFunctioncallStatement(OurJavaParser.FunctioncallStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varassigmentStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterVarassigmentStatement(OurJavaParser.VarassigmentStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varassigmentStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitVarassigmentStatement(OurJavaParser.VarassigmentStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code vardeclarationStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterVardeclarationStatement(OurJavaParser.VardeclarationStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code vardeclarationStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitVardeclarationStatement(OurJavaParser.VardeclarationStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(OurJavaParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link OurJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(OurJavaParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#variable_assigment}.
	 * @param ctx the parse tree
	 */
	void enterVariable_assigment(OurJavaParser.Variable_assigmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#variable_assigment}.
	 * @param ctx the parse tree
	 */
	void exitVariable_assigment(OurJavaParser.Variable_assigmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#return_statement}.
	 * @param ctx the parse tree
	 */
	void enterReturn_statement(OurJavaParser.Return_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#return_statement}.
	 * @param ctx the parse tree
	 */
	void exitReturn_statement(OurJavaParser.Return_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterNegationExpression(OurJavaParser.NegationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitNegationExpression(OurJavaParser.NegationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expbodyExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterExpbodyExpression(OurJavaParser.ExpbodyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expbodyExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitExpbodyExpression(OurJavaParser.ExpbodyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funccallExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterFunccallExpression(OurJavaParser.FunccallExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funccallExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitFunccallExpression(OurJavaParser.FunccallExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpression(OurJavaParser.RelationalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpression(OurJavaParser.RelationalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valuesExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterValuesExpression(OurJavaParser.ValuesExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valuesExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitValuesExpression(OurJavaParser.ValuesExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(OurJavaParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(OurJavaParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plusandminusExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterPlusandminusExpression(OurJavaParser.PlusandminusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plusandminusExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitPlusandminusExpression(OurJavaParser.PlusandminusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code minusPlusExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterMinusPlusExpression(OurJavaParser.MinusPlusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code minusPlusExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitMinusPlusExpression(OurJavaParser.MinusPlusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logicalExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpression(OurJavaParser.LogicalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logicalExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpression(OurJavaParser.LogicalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterTernaryExpression(OurJavaParser.TernaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitTernaryExpression(OurJavaParser.TernaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multanddivExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void enterMultanddivExpression(OurJavaParser.MultanddivExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multanddivExpression}
	 * labeled alternative in {@link OurJavaParser#expression_body}.
	 * @param ctx the parse tree
	 */
	void exitMultanddivExpression(OurJavaParser.MultanddivExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#variable_declaration}.
	 * @param ctx the parse tree
	 */
	void enterVariable_declaration(OurJavaParser.Variable_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#variable_declaration}.
	 * @param ctx the parse tree
	 */
	void exitVariable_declaration(OurJavaParser.Variable_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#decimal_variable}.
	 * @param ctx the parse tree
	 */
	void enterDecimal_variable(OurJavaParser.Decimal_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#decimal_variable}.
	 * @param ctx the parse tree
	 */
	void exitDecimal_variable(OurJavaParser.Decimal_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#bool_variable}.
	 * @param ctx the parse tree
	 */
	void enterBool_variable(OurJavaParser.Bool_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#bool_variable}.
	 * @param ctx the parse tree
	 */
	void exitBool_variable(OurJavaParser.Bool_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#parallel_declaration}.
	 * @param ctx the parse tree
	 */
	void enterParallel_declaration(OurJavaParser.Parallel_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#parallel_declaration}.
	 * @param ctx the parse tree
	 */
	void exitParallel_declaration(OurJavaParser.Parallel_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(OurJavaParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(OurJavaParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(OurJavaParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(OurJavaParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#boolean_literal}.
	 * @param ctx the parse tree
	 */
	void enterBoolean_literal(OurJavaParser.Boolean_literalContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#boolean_literal}.
	 * @param ctx the parse tree
	 */
	void exitBoolean_literal(OurJavaParser.Boolean_literalContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#integer_literal}.
	 * @param ctx the parse tree
	 */
	void enterInteger_literal(OurJavaParser.Integer_literalContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#integer_literal}.
	 * @param ctx the parse tree
	 */
	void exitInteger_literal(OurJavaParser.Integer_literalContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#for_type}.
	 * @param ctx the parse tree
	 */
	void enterFor_type(OurJavaParser.For_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#for_type}.
	 * @param ctx the parse tree
	 */
	void exitFor_type(OurJavaParser.For_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#for_init}.
	 * @param ctx the parse tree
	 */
	void enterFor_init(OurJavaParser.For_initContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#for_init}.
	 * @param ctx the parse tree
	 */
	void exitFor_init(OurJavaParser.For_initContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#for_statement}.
	 * @param ctx the parse tree
	 */
	void enterFor_statement(OurJavaParser.For_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#for_statement}.
	 * @param ctx the parse tree
	 */
	void exitFor_statement(OurJavaParser.For_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#switch_block}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_block(OurJavaParser.Switch_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#switch_block}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_block(OurJavaParser.Switch_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#function_declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunction_declaration(OurJavaParser.Function_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#function_declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunction_declaration(OurJavaParser.Function_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#function_header}.
	 * @param ctx the parse tree
	 */
	void enterFunction_header(OurJavaParser.Function_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#function_header}.
	 * @param ctx the parse tree
	 */
	void exitFunction_header(OurJavaParser.Function_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#function_declarator}.
	 * @param ctx the parse tree
	 */
	void enterFunction_declarator(OurJavaParser.Function_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#function_declarator}.
	 * @param ctx the parse tree
	 */
	void exitFunction_declarator(OurJavaParser.Function_declaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#function_type}.
	 * @param ctx the parse tree
	 */
	void enterFunction_type(OurJavaParser.Function_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#function_type}.
	 * @param ctx the parse tree
	 */
	void exitFunction_type(OurJavaParser.Function_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#function_body}.
	 * @param ctx the parse tree
	 */
	void enterFunction_body(OurJavaParser.Function_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#function_body}.
	 * @param ctx the parse tree
	 */
	void exitFunction_body(OurJavaParser.Function_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#formal_parameter}.
	 * @param ctx the parse tree
	 */
	void enterFormal_parameter(OurJavaParser.Formal_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#formal_parameter}.
	 * @param ctx the parse tree
	 */
	void exitFormal_parameter(OurJavaParser.Formal_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#function_call_statement}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call_statement(OurJavaParser.Function_call_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#function_call_statement}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call_statement(OurJavaParser.Function_call_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link OurJavaParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void enterArgument_list(OurJavaParser.Argument_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link OurJavaParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void exitArgument_list(OurJavaParser.Argument_listContext ctx);
}