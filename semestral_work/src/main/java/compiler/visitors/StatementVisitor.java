package main.java.compiler.visitors;

import main.java.SemanticError;
import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;
import main.java.compiler.enums.ErrorType;
import main.java.compiler.enums.ReturnType;
import main.java.compiler.enums.VariableType;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.SymbolTable;
import main.java.pl_0_part.enums.InstructionType;
import main.java.pl_0_part.model.FunctionField;
import main.java.pl_0_part.model.Instruction;
import main.java.pl_0_part.model.VariableField;
import main.java.trackers.StackTracker;
import main.java.trackers.VisitorTracker;

import java.util.List;

public class StatementVisitor extends OurJavaBaseVisitor<Void> {
	
	@Override
    public Void visitFunction_call_statement(OurJavaParser.Function_call_statementContext ctx) {
        String identifier = ctx.IDENTIFIER().getText();
        
        FunctionField function = SymbolTable.getInstance().getFunction(identifier);
        
        if (function == null) {
        	SemanticError.printError(ErrorType.ERROR_CALLED_FUNCTION_DOES_NOT_EXIST);
        }
        
        if (ctx.argument_list().size() != function.getParametersCount()) {
        	SemanticError.printError(ErrorType.ERROR_PARAMETERS_COUNT_DOES_NOT_MATCH);
        }
        
        if (function.returnType != ReturnType.VOID)
        	InstructionList.getInstance().addInstruction(InstructionType.INT, 0, 1);
        
        for (int i = ctx.argument_list().size() - 1; i >= 0; --i) {
        	new ExpressionVisitor().visit(ctx.argument_list(i));
        }
        
        /*for (OurJavaParser.Argument_listContext ctxx : ctx.argument_list()) {
        	new ExpressionVisitor().visit(ctxx);
        }*/
        
        InstructionList.getInstance().addInstruction(InstructionType.CAL, StackTracker.getInstance().getInstructionLevel(), function.getStartAddress());
        
        if (function.getParametersCount() > 0)
        	InstructionList.getInstance().addInstruction(InstructionType.INT, 0, -function.getParametersCount());
        
        return null;
    }
	
    @Override
    public Void visitFunctioncallStatement(OurJavaParser.FunctioncallStatementContext ctx) {
    	this.visit(ctx.function_call_statement());
        
    	FunctionField function = SymbolTable.getInstance().getFunction(ctx.function_call_statement().IDENTIFIER().getText());
    	
    	if (function.returnType != ReturnType.VOID)
    		InstructionList.getInstance().addInstruction(InstructionType.INT, 0, -1);
    	
        return null;
    }

    @Override
    public Void visitSwitchStatement(OurJavaParser.SwitchStatementContext ctx) {
    	List<OurJavaParser.Switch_blockContext> list = ctx.switch_block();
    	
    	Instruction instBreak = new Instruction(InstructionType.JMP, 0, 0, 0);
    	Instruction jmp = new Instruction(InstructionType.JMP, 0, 0, 0);
    	
    	int count1 = SymbolTable.getInstance().getLevelVariableCount();
    	
    	for (OurJavaParser.Switch_blockContext ctxx : list) {
    		new ExpressionVisitor().visit(ctx.expression().expression_body());
    		InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, Integer.valueOf(ctxx.DecimalNumeral().getText()));
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 8);
    		Instruction instruction = new Instruction(InstructionType.JMC, 0, 0, 0);
    		InstructionList.getInstance().addInstructionObject(instruction);
    		
    		jmp.address = InstructionList.getInstance().instructionCount;
    		
    		List<OurJavaParser.Switch_body_statementContext> statementContexts = ctxx.switch_body().switch_body_statement();

    		for (OurJavaParser.Switch_body_statementContext ctxxx : statementContexts) {
    			if (ctxxx.statement() != null) {
    				new StatementVisitor().visit(ctxxx.statement());
    			}
    			else {
    				InstructionList.getInstance().addInstructionObject(instBreak);
    			}
    		}
    		
    		jmp = new Instruction(InstructionType.JMP, 0, 0, 0);
    		InstructionList.getInstance().addInstructionObject(jmp);
    		
    		instruction.address = InstructionList.getInstance().instructionCount;
    	}
    	
    	int count2 = SymbolTable.getInstance().getLevelVariableCount();
    	
    	int count = count2 - count1;
		
		if (count > 0) {
			SymbolTable.getInstance().removeLastI(count);
			InstructionList.getInstance().addInstruction(InstructionType.INT, 0, -count);
		}
    	
    	jmp.address = InstructionList.getInstance().instructionCount;
    	instBreak.address = InstructionList.getInstance().instructionCount;
    	
    	return null;
    }
    
    @Override
    public Void visitRepeatuntilStatement(OurJavaParser.RepeatuntilStatementContext ctx) {

    	int addr = InstructionList.getInstance().instructionCount;
    	
    	if (ctx.body().block_body() != null) {
    		new BlockBodyVisitor().visit(ctx.body().block_body());
    	}
    	
        new ExpressionVisitor().visit(ctx.expression().expression_body());
        
        InstructionList.getInstance().addInstruction(InstructionType.JMC, 0, addr);
    	
    	
        return null;
    }

    @Override
    public Void visitDowhileStatement(OurJavaParser.DowhileStatementContext ctx) {

    	int addr = InstructionList.getInstance().getInstructionCount();
    	
    	if (ctx.body().block_body() != null) {
    		new BlockBodyVisitor().visit(ctx.body().block_body());
    	}
    	
        new ExpressionVisitor().visit(ctx.expression().expression_body());
        
        InstructionList.getInstance().addInstruction(InstructionType.JMC, 0, InstructionList.getInstance().instructionCount + 2);
        
        InstructionList.getInstance().addInstruction(InstructionType.JMP, 0, addr);
        
        
        return null;
    }

    @Override
    public Void visitWhileStatement(OurJavaParser.WhileStatementContext ctx) {
    	int addr = InstructionList.getInstance().instructionCount;
        new ExpressionVisitor().visit(ctx.expression().expression_body());
        Instruction inst = new Instruction(InstructionType.JMC, 0, 0, 0);
    	
    	InstructionList.getInstance().addInstructionObject(inst);

    	if (ctx.body().block_body() != null) {
    		new BlockBodyVisitor().visit(ctx.body().block_body());
    	}
    	
    	InstructionList.getInstance().addInstruction(InstructionType.JMP, 0, addr);
    	
    	inst.address = InstructionList.getInstance().instructionCount;
    	
        return null;
    }
    
    @Override
    public Void visitForStatement(OurJavaParser.ForStatementContext ctx) {
    	String forType = ctx.for_statement().for_type().getText().toUpperCase();
    	String varName = ctx.for_statement().for_init().IDENTIFIER().getText();
    	
    	
    	this.visit(ctx.for_statement().for_init());
    	
    	VariableField variable = SymbolTable.getInstance().getVariable(varName);
    	
    	int cnt = InstructionList.getInstance().instructionCount;
    	
    	this.visit(ctx.for_statement());
    	
    	InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 13);
        
        Instruction inst = new Instruction(InstructionType.JMC, 0, 0, 0);
        
        InstructionList.getInstance().addInstructionObject(inst);
    	
    	if (ctx.body().block_body() != null) {
    		new BlockBodyVisitor().visit(ctx.body().block_body());
    	}
    	
    	InstructionList.getInstance().addInstruction(InstructionType.LOD, 0, variable.getAddress());
    	InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, 1);
    	
    	if (forType.equals("TO")) {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 2);
    	}
    	else {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 3);
    	}
    	
    	InstructionList.getInstance().addInstruction(InstructionType.STO, 0, variable.getAddress());
    	
    	InstructionList.getInstance().addInstruction(InstructionType.JMP, 0, cnt);
    	inst.address = InstructionList.getInstance().instructionCount;
    	
    	SymbolTable.getInstance().removeLastI(1);
		InstructionList.getInstance().addInstruction(InstructionType.INT, 0, -1);
    	
    	return null;
    }
    
    @Override
    public Void visitFor_statement(OurJavaParser.For_statementContext ctx) {
    	
    	String varName = ctx.for_init().IDENTIFIER().getText();
    	
    	VariableField variable = SymbolTable.getInstance().getVariable(varName);
    	
    	InstructionList.getInstance().addInstruction(InstructionType.LOD, variable.getLevel(), variable.getAddress());
        
        if (ctx.integer_literal().DecimalNumeral() != null) {
            int value = Integer.parseInt(ctx.integer_literal().DecimalNumeral().getText());
            if(ctx.integer_literal().decimal_symbol()!= null && ctx.integer_literal().decimal_symbol().getText().equals("-")){
                value *= -1;
            }
            
            InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, value);
        }
        
        if (ctx.integer_literal().IDENTIFIER() != null) {
            String value = ctx.integer_literal().IDENTIFIER().getText();

            VariableField variableIdentifier = SymbolTable.getInstance().getVariable(value);
            
            if (variableIdentifier == null) {
            	SemanticError.printError(ErrorType.ERROR_VARIABLE_DOES_NOT_EXIST);
            }
            
            InstructionList.getInstance().addInstruction(InstructionType.LOD, variableIdentifier.getLevel(), variableIdentifier.getAddress());
        }
        
        if (ctx.integer_literal().expression_body() != null) {
        	new ExpressionVisitor().visit(ctx.integer_literal().expression_body());
        }
        
        if (ctx.integer_literal().function_call_statement() != null) {
        	new StatementVisitor().visit(ctx.integer_literal().function_call_statement());
        }
    	
    	return null;
    }
    
    @Override
    public Void visitFor_init(OurJavaParser.For_initContext ctx) {
    	String identifier = ctx.IDENTIFIER().getText();
    	
    	if (SymbolTable.getInstance().containsVariable(identifier)) {
    		SemanticError.printError(ErrorType.ERROR_VARIABLE_ALREADY_EXISTS);
    	}
    	
    	InstructionList.getInstance().addInstruction(InstructionType.INT, 0, 1);
    	
    	new ExpressionVisitor().visit(ctx.expression_body());
    	
    	VariableField variable = SymbolTable.getInstance().addVariable(identifier, VariableType.INT, false);
    	InstructionList.getInstance().addInstruction(InstructionType.STO, 0, variable.getAddress());
    	
    	return null;
    }

    @Override
    public Void visitIfelseStatement(OurJavaParser.IfelseStatementContext ctx) {
    	
    	new ExpressionVisitor().visit(ctx.expression().expression_body());
    	
    	Instruction inst = new Instruction(InstructionType.JMC, 0, 0, 0);
    	
    	InstructionList.getInstance().addInstructionObject(inst);
    	
        BlockBodyVisitor bbv = new BlockBodyVisitor();
        
        if (ctx.body(0).block_body() != null)
        	bbv.visit(ctx.body(0).block_body());
        
        inst.address = InstructionList.getInstance().instructionCount;
        
        if (ctx.ELSE() != null) {
        	inst.address++;
        	
        	inst = new Instruction(InstructionType.JMP, 0, 0, 0);
        	
        	InstructionList.getInstance().addInstructionObject(inst);
        	if (ctx.body(1).block_body() != null)
        		bbv.visit(ctx.body(1).block_body());
        	inst.address = InstructionList.getInstance().instructionCount;
        }
        
        
        return null;
    }

    @Override
    public Void visitVariable_declaration(OurJavaParser.Variable_declarationContext ctx)
    {
        if (ctx.decimal_variable() != null) {
            new VariableVisitor().visitDecimal_variable(ctx.decimal_variable());
        }
        
        if (ctx.bool_variable() != null) {
            new VariableVisitor().visitBool_variable(ctx.bool_variable());
        }
        
        return null;
    }

    @Override
    public Void visitVarassigmentStatement(OurJavaParser.VarassigmentStatementContext ctx) {
        String identifier = ctx.variable_assigment().IDENTIFIER().getText();
        
        VariableField variable = SymbolTable.getInstance().getVariable(identifier);
        
        if (variable == null) {
        	SemanticError.printError(ErrorType.ERROR_VARIABLE_DOES_NOT_EXIST);
        }

        if (variable.isConstant()) {
        	SemanticError.printError(ErrorType.ERROR_ASSIGNED_VALUE_TO_CONST);
        }

        new ExpressionVisitor().visit(ctx.variable_assigment().expression_body());

        InstructionList.getInstance().addInstruction(InstructionType.STO, variable.getLevel(), variable.getAddress());
        
        return null;
    }
    
    @Override
    public Void visitReturn_statement(OurJavaParser.Return_statementContext ctx) {
    	if (StackTracker.getInstance().getInstructionLevel() == 0) {
    		InstructionList.getInstance().addInstruction(InstructionType.RET, 0, 0);
    		return null;
    	}
    	
    	String funcName = VisitorTracker.getInstance().getFunctionName();
    	
    	if (!SymbolTable.getInstance().containsFunction(funcName))
    		return null;
    	
    	FunctionField function = SymbolTable.getInstance().getFunction(funcName);
    	
    	if (function.getReturnType() == ReturnType.VOID && ctx.expression_body() != null) {
    		SemanticError.printError(ErrorType.ERROR_RETURNING_VALUE_IN_VOID);
    	}
    	
    	if (function.getReturnType() != ReturnType.VOID && ctx.expression_body() == null) {
    		SemanticError.printError(ErrorType.ERROR_NOT_RETURNING_VALUE_IN_RETURN_STATEMENT);
    	}
    	
    	if (ctx.expression_body() != null)
    		new ExpressionVisitor().visit(ctx.expression_body());
    	
    	if (function.getReturnType() != ReturnType.VOID) {
    		InstructionList.getInstance().addInstruction(InstructionType.STO, 0, function.getReturnAddress());
    	}
    	
    	InstructionList.getInstance().addInstruction(InstructionType.RET, 0, 0);
    	
    	return null;
    }
}
