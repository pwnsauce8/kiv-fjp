package main.java.compiler.visitors;

import java.util.ArrayList;
import java.util.List;

import main.java.SemanticError;
import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;
import main.java.compiler.enums.ErrorType;
import main.java.compiler.enums.ReturnType;
import main.java.compiler.enums.VariableType;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.SymbolTable;
import main.java.pl_0_part.enums.InstructionType;
import main.java.pl_0_part.model.Instruction;
import main.java.pl_0_part.model.VariableField;
import main.java.trackers.VisitorTracker;

public class FunctionDeclarationVisitor extends OurJavaBaseVisitor<Void> {
	
	@Override
	public Void visitFunction_declaration(OurJavaParser.Function_declarationContext ctx) {
		Instruction instruction = new Instruction(InstructionType.JMP, 0, 0, 0);
		InstructionList.getInstance().addInstructionObject(instruction);
		
		ReturnType retType = ReturnType.valueOf(ctx.function_header().function_type().getText().toUpperCase());
		
		String name = ctx.function_header().function_declarator().IDENTIFIER().getText();
		
		VisitorTracker.getInstance().startVisitingFunction(name);
		
		int line = InstructionList.getInstance().getInstructionCount();
		
		List<OurJavaParser.Formal_parameterContext> paramContext = ctx.function_header().function_declarator().formal_parameter();
		
		List<VariableType> paramTypes = new ArrayList<>();
		
		InstructionList.getInstance().addInstruction(InstructionType.INT, 0, 3);
		
		int i = 1;
		
		for (OurJavaParser.Formal_parameterContext param : paramContext) {
			VariableType varType = VariableType.valueOf(param.varTypes().getText().toUpperCase());
			paramTypes.add(varType);
			
			if (SymbolTable.getInstance().containsVariable(param.IDENTIFIER().getText())) {
				SemanticError.printError(ErrorType.ERROR_VARIABLE_ALREADY_EXISTS);
			}
			
			VariableField variable = SymbolTable.getInstance().addVariable(param.IDENTIFIER().getText(), varType, false);
			
			InstructionList.getInstance().addInstruction(InstructionType.INT, 0, 1);
			InstructionList.getInstance().addInstruction(InstructionType.LOD, 0, -i++);
			InstructionList.getInstance().addInstruction(InstructionType.STO, 0, variable.getAddress());
		}
		
		SymbolTable.getInstance().addFunction(name, retType, paramTypes, line);
		
		if (ctx.function_body().block_body() != null) {
			
			new BlockBodyVisitor().visit(ctx.function_body().block_body());
			
			List<OurJavaParser.StatementContext> list = ctx.function_body().block_body().statement();
			
			if (!list.get(list.size() - 1).start.getText().equals("return") && retType != ReturnType.VOID) {
				SemanticError.printError(ErrorType.ERROR_DOES_NOT_CONTAINS_RETURN_STATEMENT);
			}
			
			if (!list.get(list.size() - 1).start.getText().equals("return") && retType == ReturnType.VOID) {
				InstructionList.getInstance().addInstruction(InstructionType.RET, 0, 0);
			}
		}
		else if (retType != ReturnType.VOID) {
			SemanticError.printError(ErrorType.ERROR_DOES_NOT_CONTAINS_RETURN_STATEMENT);
		}
		else {
			InstructionList.getInstance().addInstruction(InstructionType.RET, 0, 0);
		}
		
		if (i > 2)
			SymbolTable.getInstance().removeLastI(i - 1);
		
		VisitorTracker.getInstance().stopVisitingFunction();
		
		instruction.address = InstructionList.getInstance().instructionCount;
		
		return null;
	}
}
