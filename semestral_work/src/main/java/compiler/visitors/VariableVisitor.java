package main.java.compiler.visitors;

import main.java.SemanticError;
import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;
import main.java.compiler.enums.ErrorType;
import main.java.compiler.enums.ReturnType;
import main.java.compiler.enums.VariableType;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.SymbolTable;
import main.java.pl_0_part.enums.InstructionType;
import main.java.pl_0_part.model.FunctionField;
import main.java.pl_0_part.model.VariableField;

public class VariableVisitor extends OurJavaBaseVisitor<Void> {

    @Override
    public Void visitBool_variable(OurJavaParser.Bool_variableContext ctx) {
        boolean  modifier = ctx.modifier() != null;
        String identifier = ctx.IDENTIFIER().getText();
        
        int numOfVars = 1;
        
        if (SymbolTable.getInstance().containsVariable(identifier)) {
        	SemanticError.printError(ErrorType.ERROR_VARIABLE_ALREADY_EXISTS);
        }
        
        for (OurJavaParser.Parallel_declarationContext ctxx : ctx.parallel_declaration()) {
        	if (SymbolTable.getInstance().containsVariable(ctxx.IDENTIFIER().getText())) {
            	SemanticError.printError(ErrorType.ERROR_VARIABLE_ALREADY_EXISTS);
            }
        	
        	++numOfVars;
        }
        
        InstructionList.getInstance().addInstruction(InstructionType.INT, 0, numOfVars);

        if (ctx.boolean_literal().TRUE() != null || ctx.boolean_literal().FALSE() != null) {
            boolean value = ctx.boolean_literal().TRUE() != null ? Boolean.valueOf(ctx.boolean_literal().TRUE().getText()) : Boolean.valueOf(ctx.boolean_literal().FALSE().getText());
            
            InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, value ? 1 : 0);
        }

        if (ctx.boolean_literal().IDENTIFIER() != null) {
        	String value = ctx.boolean_literal().IDENTIFIER().getText();

            VariableField variable = SymbolTable.getInstance().getVariable(value);
            
            if (variable == null) {
            	SemanticError.printError(ErrorType.ERROR_VARIABLE_DOES_NOT_EXIST);
            }
            
            InstructionList.getInstance().addInstruction(InstructionType.LOD, 0, variable.getAddress());
        }
        
        if (ctx.boolean_literal().expression_body() != null) {
        	new ExpressionVisitor().visit(ctx.boolean_literal().expression_body());
        }
        
        if (ctx.boolean_literal().function_call_statement() != null) {
        	String functionName = ctx.boolean_literal().function_call_statement().IDENTIFIER().getText();
        	
        	FunctionField function = SymbolTable.getInstance().getFunction(functionName);
        	
        	new StatementVisitor().visit(ctx.boolean_literal().function_call_statement());
        	
        	if (function.getReturnType() == ReturnType.VOID) {
        		SemanticError.printError(ErrorType.ERROR_VOID_INSIDE_ASSIGMENT);
        	}
        }
        
        VariableField variable = SymbolTable.getInstance().addVariable(identifier, VariableType.BOOLEAN, modifier);
        InstructionList.getInstance().addInstruction(InstructionType.STO, 0, variable.getAddress());
        
        for (OurJavaParser.Parallel_declarationContext ctxx : ctx.parallel_declaration()) {
            VariableField nextVariable = SymbolTable.getInstance().addVariable(ctxx.IDENTIFIER().getText(), VariableType.BOOLEAN, modifier);
            
            InstructionList.getInstance().addInstruction(InstructionType.LOD, 0, variable.getAddress());
            InstructionList.getInstance().addInstruction(InstructionType.STO, 0, nextVariable.getAddress());
        }
        
        return null;
    }

    @Override
    public Void visitDecimal_variable(OurJavaParser.Decimal_variableContext ctx) {
        boolean  modifier = ctx.modifier() != null;
        String identifier = ctx.IDENTIFIER().getText();
        
        int numOfVars = 1;
        
        if (SymbolTable.getInstance().containsVariable(identifier)) {
        	SemanticError.printError(ErrorType.ERROR_VARIABLE_ALREADY_EXISTS);
        }
        
        for (OurJavaParser.Parallel_declarationContext ctxx : ctx.parallel_declaration()) {
        	if (SymbolTable.getInstance().containsVariable(ctxx.IDENTIFIER().getText())) {
            	SemanticError.printError(ErrorType.ERROR_VARIABLE_ALREADY_EXISTS);
            }
        	++numOfVars;
        }
        
        InstructionList.getInstance().addInstruction(InstructionType.INT, 0, numOfVars);
        
        if (ctx.integer_literal().DecimalNumeral() != null) {
            int value = Integer.parseInt(ctx.integer_literal().DecimalNumeral().getText());
            if(ctx.integer_literal().decimal_symbol() != null && ctx.integer_literal().decimal_symbol().getText().equals("-")){
                value *= -1;
            }

            InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, value);
        }
        
        if (ctx.integer_literal().IDENTIFIER() != null) {
        	String value = ctx.integer_literal().IDENTIFIER().getText();

            VariableField variable = SymbolTable.getInstance().getVariable(value);
            
            if (variable == null) {
            	SemanticError.printError(ErrorType.ERROR_VARIABLE_DOES_NOT_EXIST);
            }
            
            InstructionList.getInstance().addInstruction(InstructionType.LOD, variable.getLevel(), variable.getAddress());
        }
        
        if (ctx.integer_literal().expression_body() != null) {
        	new ExpressionVisitor().visit(ctx.integer_literal().expression_body());
        }
        
        if (ctx.integer_literal().function_call_statement() != null) {
        	String functionName = ctx.integer_literal().function_call_statement().IDENTIFIER().getText();
        	
        	FunctionField function = SymbolTable.getInstance().getFunction(functionName);
        	
        	new StatementVisitor().visit(ctx.integer_literal().function_call_statement());
        	
        	if (function.getReturnType() == ReturnType.VOID) {
        		SemanticError.printError(ErrorType.ERROR_VOID_INSIDE_ASSIGMENT);
        	}
        }
        
        VariableField variable = SymbolTable.getInstance().addVariable(identifier, VariableType.INT, modifier);
        
        InstructionList.getInstance().addInstruction(InstructionType.STO, 0, variable.getAddress());
        
        for (OurJavaParser.Parallel_declarationContext ctxx : ctx.parallel_declaration()) {
            VariableField nextVariable = SymbolTable.getInstance().addVariable(ctxx.IDENTIFIER().getText(), VariableType.INT, modifier);
            
            InstructionList.getInstance().addInstruction(InstructionType.LOD, 0, variable.getAddress());
            InstructionList.getInstance().addInstruction(InstructionType.STO, 0, nextVariable.getAddress());
        }
        
        return null;
    }
}
