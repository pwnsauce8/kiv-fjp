package main.java.compiler.visitors;

import java.util.List;

import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.SymbolTable;
import main.java.pl_0_part.enums.InstructionType;
import main.java.trackers.StackTracker;
import main.java.trackers.VisitorTracker;

public class BlockBodyVisitor extends OurJavaBaseVisitor<Void> {
	
	@Override
	public Void visitBlock_body(OurJavaParser.Block_bodyContext ctx) {
		if (ctx == null) {
			return null;
		}
		
		VisitorTracker.getInstance().increaseNestingLevel();
		
		int count1 = SymbolTable.getInstance().getLevelVariableCount();
		
		List<OurJavaParser.StatementContext> statementContexts = ctx.statement();

		for (OurJavaParser.StatementContext statementContext : statementContexts) {
			new StatementVisitor().visit(statementContext);
		}
		
		int count2 = SymbolTable.getInstance().getLevelVariableCount();
		
		int count = count2 - count1;
		
		VisitorTracker.getInstance().decreaseNestingLevel();
		
		int nestingLevel = VisitorTracker.getInstance().getNestingLevel();
		
		boolean removeFlag = (nestingLevel == 0) && (StackTracker.getInstance().getInstructionLevel() > 0) ? false : true;
		
		if (count > 0 && removeFlag) {
			SymbolTable.getInstance().removeLastI(count);
			InstructionList.getInstance().addInstruction(InstructionType.INT, 0, -count);
		}
		
		return null;
	}
}
