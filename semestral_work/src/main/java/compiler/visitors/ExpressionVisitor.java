package main.java.compiler.visitors;


import main.java.SemanticError;
import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;
import main.java.compiler.enums.ErrorType;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.SymbolTable;
import main.java.pl_0_part.enums.InstructionType;
import main.java.pl_0_part.model.Instruction;
import main.java.pl_0_part.model.VariableField;


public class ExpressionVisitor extends OurJavaBaseVisitor<Void> {
	
	@Override
	public Void visitFunccallExpression(OurJavaParser.FunccallExpressionContext ctx) {
		new StatementVisitor().visit(ctx.function_call_statement());
		return null;
	}
	
	@Override
	public Void visitTernaryExpression(OurJavaParser.TernaryExpressionContext ctx) {
		this.visit(ctx.expression_body(0));
		
		Instruction instruction = new Instruction(InstructionType.JMC, 0, 0, 0);
		InstructionList.getInstance().addInstructionObject(instruction);
		
		this.visit(ctx.expression_body(1));
		
		Instruction instruction1 = new Instruction(InstructionType.JMP, 0, 0, 0);
		InstructionList.getInstance().addInstructionObject(instruction1);
		
		instruction.address = InstructionList.getInstance().instructionCount;
		
		this.visit(ctx.expression_body(2));
		
		instruction1.address = InstructionList.getInstance().instructionCount;
		
		return null;
	}
	
    @Override
    public Void visitLogicalExpression(OurJavaParser.LogicalExpressionContext ctx) {
    	this.visit(ctx.expression_body(0));
    	this.visit(ctx.expression_body(1));
    	
    	if (ctx.op.getText().equals("&&")) {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 4);
    		InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, 0);
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 9);
    	}
    	else {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 2);
    		InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, 0);
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 9);
    	}
    	
        return null;
    }

    @Override
    public Void visitIdentifierExpression(OurJavaParser.IdentifierExpressionContext ctx) {
    	VariableField variable = SymbolTable.getInstance().getVariable(ctx.IDENTIFIER().getText());
    	
    	if (variable == null) {
    		SemanticError.printError(ErrorType.ERROR_VARIABLE_DOES_NOT_EXIST);
    	}
    	
    	InstructionList.getInstance().addInstruction(InstructionType.LOD, variable.getLevel(), variable.getAddress());
    	
    	return null;
    }

    @Override
    public Void visitValuesExpression(OurJavaParser.ValuesExpressionContext ctx) {

        if (ctx.values().decimal_values() != null)
        {
            int val = Integer.parseInt(ctx.values().decimal_values().getText());
            InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, val);
        }
        else if (ctx.values().boolean_values() != null)
        {
            String val = ctx.values().boolean_values().getText();
            InstructionList.getInstance().addInstruction(InstructionType.LIT, 0,  Boolean.parseBoolean(val) ? 1 : 0);
        }

        return null;
    }
    
    @Override
    public Void visitPlusandminusExpression(OurJavaParser.PlusandminusExpressionContext ctx) {
    	this.visit(ctx.expression_body(0));
    	this.visit(ctx.expression_body(1));
    	
    	if (ctx.op.getText().equals("+")) {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 2);
    	}
    	else {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 3);
    	}
    	
    	return null;
    }

    @Override
    public Void visitMultanddivExpression(OurJavaParser.MultanddivExpressionContext ctx) {
    	this.visit(ctx.expression_body(0));
    	this.visit(ctx.expression_body(1));
    	
    	if (ctx.op.getText().equals("*")) {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 4);
    	}
    	else {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 5);
    	}
    	
    	return null;
    }
    
    @Override
    public Void visitNegationExpression(OurJavaParser.NegationExpressionContext ctx) {
    	/*if (BaseClass.getInstance().visitingInt) {
    		SemanticError.printError("Using negation operator inside variable declaration of type INT");
    	}*/
    	
    	this.visit(ctx.expression_body());
    	
    	InstructionList.getInstance().addInstruction(InstructionType.LIT, 0, 0);
    	
    	InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 8);
    	
    	return null;
    }
    
    @Override
    public Void visitMinusPlusExpression(OurJavaParser.MinusPlusExpressionContext ctx) {
    	this.visit(ctx.expression_body());
    	
    	if (ctx.MINUS() != null) {
    		InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 1);
    	}
    	
    	return null;
    }
    
    public Void visitRelationalExpression(OurJavaParser.RelationalExpressionContext ctx) {
    	
    	this.visit(ctx.expression_body(0));
    	this.visit(ctx.expression_body(1));
    	
    	switch (ctx.op.getText()) {
    	case "==": InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 8);
    	break;
    	case "!=": InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 9);
    	break;
    	case "<": InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 10);
    	break;
    	case ">=": InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 11);
    	break;
    	case ">": InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 12);
    	break;
    	case "<=": InstructionList.getInstance().addInstruction(InstructionType.OPR, 0, 13);
    	break;
    	}
    	
    	return null;
    }
}
