package main.java.compiler.visitors;

import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;

public class BlockStatementsVisitor extends OurJavaBaseVisitor<Void> {


    public Void visitBlockStatement(OurJavaParser.Block_statementContext ctx) {

    	if (ctx == null)
    		return null;
    	
    	for (OurJavaParser.StatementsContext ctxx : ctx.statements()) {
    		if (ctxx.statement() != null) {
    			new StatementVisitor().visit(ctxx.statement());
    		}
    		else {
    			new FunctionDeclarationVisitor().visit(ctxx.function_declaration());
    		}
    	}
    	
        return null;
    }

}
