package main.java.compiler.visitors;

import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;
import main.java.pl_0_part.InstructionList;
import main.java.pl_0_part.enums.InstructionType;
import main.java.trackers.StackTracker;

public class ProgramVisitor extends OurJavaBaseVisitor<Void> {

    @Override
    public Void visitProgram(OurJavaParser.ProgramContext ctx) {
        InstructionList.getInstance().addInstruction(InstructionType.JMP, 0, 1);
        InstructionList.getInstance().addInstruction(InstructionType.INT, 0, StackTracker.getInstance().ACTIVATION_RECORD_SIZE);
        
        if (ctx != null)
        	new BlockStatementsVisitor().visitBlockStatement(ctx.block().block_statement());
        
        InstructionList.getInstance().addInstruction(InstructionType.RET, 0,0);
        
        return null;
    }
}