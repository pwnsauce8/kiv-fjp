package main.java.compiler.visitors;

import java.util.List;

import main.java.antlr4.OurJavaBaseVisitor;
import main.java.antlr4.OurJavaParser;

public class SwitchBodyVisitor extends OurJavaBaseVisitor<Void> {
	@Override
	public Void visitSwitch_body(OurJavaParser.Switch_bodyContext ctx) {
		if (ctx == null) {
			return null;
		}
		
		List<OurJavaParser.Switch_body_statementContext> statementContexts = ctx.switch_body_statement();

		for (OurJavaParser.Switch_body_statementContext ctxx : statementContexts) {
			if (ctxx.statement() != null)
				new StatementVisitor().visit(ctxx.statement());
		}

		return null;
	}
}
