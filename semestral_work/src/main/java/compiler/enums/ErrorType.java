package main.java.compiler.enums;

public enum ErrorType {
	ERROR_VARIABLE_DOES_NOT_EXIST("Variable does not exist"),
	ERROR_VARIABLE_ALREADY_EXISTS("Variable already exists"),
	ERROR_ASSIGNED_VALUE_TO_CONST("Assigning value to constant"),
	ERROR_RETURNING_VALUE_IN_VOID("Returning value in void function"),
	ERROR_NOT_RETURNING_VALUE_IN_RETURN_STATEMENT("Not returning value in return statement"),
	ERROR_PARAMETERS_COUNT_DOES_NOT_MATCH("Number of function parameters does not match"),
	ERROR_CALLED_FUNCTION_DOES_NOT_EXIST("Called function does not exist"),
	ERROR_VOID_INSIDE_ASSIGMENT("Var assigment contains call of function that should not return value"),
	ERROR_DOES_NOT_CONTAINS_RETURN_STATEMENT("Function that should return value does not contains return statement."),
	;
	
	private String text;
	
	private ErrorType(String text) {
		this.text = text;
	}
	
	public String getMsg() {
		return this.text;
	}
}
