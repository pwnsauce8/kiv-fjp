package main.java.compiler.enums;

public enum ReturnType {
    INT,
    BOOLEAN,
    VOID
}
