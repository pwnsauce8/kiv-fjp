package main.java;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;

public class LexerParserErrorListener extends BaseErrorListener
{
    private static final LexerParserErrorListener instance = new LexerParserErrorListener();

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            throws ParseCancellationException
    {
        System.out.println("\nError during parsing at line " + line + ", position " + charPositionInLine + ".");
        System.out.println("Error message: " + msg + ".");
        System.exit(1);
    }

    public static LexerParserErrorListener getInstance()
    {
        return instance;
    }

}
