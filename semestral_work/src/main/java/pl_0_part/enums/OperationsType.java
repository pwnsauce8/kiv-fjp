package main.java.pl_0_part.enums;

public enum OperationsType {
    RETURN(0),
    NEGATION(1),
    ADDITION(2),
    SUBTRACTION(3),
    MULTIPLICATION(4),
    DIVISION(5),
    IS_ODD(6),
    MODULUS(7),
    EQUALITY(8),
    INEQUALITY(9),
    LESS_THAN(10),
    LESS_THAN_OR_EQUAL(11),
    GREATER_THAN(12),
    GREATER_THAN_OR_EQUAL(13);

    private final int code;

    OperationsType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
