package main.java.pl_0_part.enums;

public enum InstructionType {
    LIT, // Push constant value (literal) M onto the stack
    OPR, // Operation to be performed on the data at the top of the stack
    LOD, // Load value to top of stack from the stack location at offset M from L lexicographical levels down
    STO, // Store value at top of stack in the stack location at offset M from L lexicographical levels down
    CAL, // Call procedure at code index M
    INT,
    JMP, // Jump to instruction M
    JMC; // Pop the top of the stack and jump to instruction M if it is equal to zero
}
