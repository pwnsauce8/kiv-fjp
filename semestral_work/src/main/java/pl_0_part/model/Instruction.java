package main.java.pl_0_part.model;

import main.java.pl_0_part.enums.InstructionType;

public class Instruction {
    private InstructionType instruction;
    public int instructionNumber;
    private int level;
    public int address;

    public Instruction(InstructionType instruction, int instructionNumber, int level, int address)
    {
        this.instruction = instruction;
        this.instructionNumber = instructionNumber;
        this.level = level;
        this.address = address;
    }

    public void setInstructionNumber(int instructionNumber) {
        this.instructionNumber = instructionNumber;
    }

    @Override
    public String toString()
    {
        return this.instructionNumber + "\t" + this.instruction + "\t" + this.level + "\t" + this.address + "\n";
    }
}
