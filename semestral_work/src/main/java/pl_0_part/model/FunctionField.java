package main.java.pl_0_part.model;

import java.util.List;

import main.java.compiler.enums.ReturnType;
import main.java.compiler.enums.VariableType;

public class FunctionField {
	public String name;
	public ReturnType returnType;
	public List<VariableType> paramTypes;
	public int startingAddress;
	
	public FunctionField(String name, ReturnType returnType, List<VariableType> paramTypes, int startingAddress) {
		this.name = name;
		this.returnType = returnType;
		this.paramTypes = paramTypes;
		this.startingAddress = startingAddress;
	}
	
	public int getParametersCount() {
		return this.paramTypes.size();
	}
	
	public int getStartAddress() {
		return this.startingAddress;
	}
	
	public ReturnType getReturnType() {
		return this.returnType;
	}
	
	public int getReturnAddress() {
		return -(paramTypes.size() + 1);
	}
}
