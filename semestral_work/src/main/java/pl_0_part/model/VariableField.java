package main.java.pl_0_part.model;

import main.java.compiler.enums.VariableType;
import main.java.trackers.StackTracker;

public class VariableField {
	public String name;
	public VariableType variableType;
	public boolean isConstant;
	public int address;
	public int level;
	
	public VariableField(String name, VariableType variableType, boolean isConstant, int address, int level) {
		this.name = name;
		this.variableType = variableType;
		this.isConstant = isConstant;
		this.address = address;
		this.level = level;
	}
	
	public int getAddress() {
		return this.address;
	}
	
	public int getLevel() {
		return (level + StackTracker.getInstance().getInstructionLevel()) % 2;
	}
	
	public boolean isConstant() {
		return isConstant;
	}
}
