package main.java.pl_0_part;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.java.compiler.enums.ReturnType;
import main.java.compiler.enums.VariableType;
import main.java.pl_0_part.model.FunctionField;
import main.java.pl_0_part.model.VariableField;
import main.java.trackers.StackTracker;

public class SymbolTable {
	private static SymbolTable instance = new SymbolTable();
	
	public Map<String, VariableField> variableGlobalTable;
	public Map<String, VariableField> variableLocalTable;
	public Map<String, FunctionField> functionTable;
	
	public List<String> variableGlobalList;
	public List<String> variableLocalList;
	
	private SymbolTable() {
		this.variableGlobalTable = new HashMap<>();
		this.variableLocalTable = new HashMap<>();
		this.functionTable = new HashMap<>();
		this.variableGlobalList = new ArrayList<>();
		this.variableLocalList = new ArrayList<>();
	}
	
	public static SymbolTable getInstance() {
		return instance;
	}
	
	public VariableField addVariable(String name, VariableType variableType, boolean isConstant) {
		int address = StackTracker.getInstance().getAndIncreaseStackPointer();
		int level = StackTracker.getInstance().getInstructionLevel();
		
		VariableField variable = new VariableField(name, variableType, isConstant, address, level);;
		
		if (StackTracker.getInstance().getInstructionLevel() > 0) {
			variableLocalTable.put(name, variable);
			variableLocalList.add(name);
		}
		else {
			variableGlobalTable.put(name, variable);
			variableGlobalList.add(name);
		}
		
		return variable;
	}
	
	/**
	 * If instruction level is higher than 0 (== visiting function) then
	 * this method returns true only when variable with name specified
	 * by param "name" already exists inside function body
	 */
	public boolean containsVariable(String name) {
		if (StackTracker.getInstance().getInstructionLevel() > 0) {
			return variableLocalTable.containsKey(name);
		}
		
		return variableGlobalTable.containsKey(name);
	}
	
	/**
	 * If instruction level is higher than 0 (== visiting function) then
	 * return local variable field (if exists) or global variable field
	 * if local variable field does not exists (can also return null if
	 * neither of them exist) 
	 */
	public VariableField getVariable(String name) {
		if (StackTracker.getInstance().getInstructionLevel() > 0) {
			VariableField variableField = variableLocalTable.get(name);
			
			return variableField == null ? variableGlobalTable.get(name) : variableField;
		}
		
		return variableGlobalTable.get(name);
	}
	
	public void addFunction(String name, ReturnType returnType, List<VariableType> params, int startingAddress) {
		functionTable.put(name, new FunctionField(name, returnType, params, startingAddress));
	}
	
	public boolean containsFunction(String name) {
		return functionTable.containsKey(name);
	}
	
	public FunctionField getFunction(String name) {
		return functionTable.get(name);
	}
	
	public int getLevelVariableCount() {
		if (StackTracker.getInstance().getInstructionLevel() > 0) {
			return variableLocalList.size();
		}
		return variableGlobalList.size();
	}
	
	public void removeLastI(int i) {
		List<String> list = null;
		Map<String, VariableField> map = null;
		
		if (StackTracker.getInstance().getInstructionLevel() > 0) {
			map = variableLocalTable;
			list = this.variableLocalList;
		}
		else {
			map = variableGlobalTable;
			list = this.variableGlobalList;
		}
		
		for (; i > 0; --i) {
			String name = list.get(list.size() - i);
			list.remove(list.size() - i);
			map.remove(name);
			StackTracker.getInstance().decreaseStackPointer();
		}
	}
}
