package main.java.pl_0_part;

import main.java.pl_0_part.enums.InstructionType;
import main.java.pl_0_part.model.Instruction;

import java.util.ArrayList;
import java.util.List;

public class InstructionList {

    //public static int stackPointer = 3;
    //FNC SP
    //public static int functionStackPointer = 3;
    
    private static InstructionList single_instance = null;
    
    private List<Instruction> instructionList;
    
    public int instructionCount;

    private InstructionList() {
        this.instructionList = new ArrayList<>();
        instructionCount = 0;
    }

    public static InstructionList getInstance()
    {
        if (single_instance == null)
            single_instance = new InstructionList();
        return single_instance;
    }

    public void addInstruction(InstructionType type, int level, int address) {
        this.instructionList.add(new Instruction(type, instructionCount, level, address));
        instructionCount++;
    }

    public Instruction createInstruction(InstructionType type, int level, int address) {
        return new Instruction(type, instructionCount, level, address);
    }

    public List<Instruction> getInstructionList() {
        return instructionList;
    }

    /*private void increaseStackPointer()
    {
    	if (BaseClass.getInstance().instruction_level > 0)
    		functionStackPointer++;
    	else
    		stackPointer++;
    }

    public int getAndIncreaseStackPointer()
    {
    	int val = 0;
    	if (BaseClass.getInstance().instruction_level > 0) {
    		val = functionStackPointer;
            this.increaseStackPointer();
    	}
    	else {
    		val = stackPointer;
            this.increaseStackPointer();
    	}

        return val;
    }*/
    
    public void addInstructionObject(Instruction instruction) {
    	instruction.instructionNumber = instructionCount;
    	this.instructionList.add(instruction);
    	++instructionCount;
    }
    
    public int getInstructionCount() {
    	return this.instructionCount;
    }
}
