{
	int x = 3;
	
	while (x < 5) {
		x = x + 1;
	}
	
	// zde je vysledek 5
	
	do {
		x = x + 1;
	} while (x < 5)
	
	// zde je vysledek 6
	
	repeat {
		x = x + 1;
	} until (x > 7)
	
	// zde je vysledek 8
	
	for (int i = x to 9) {
		x = x + 1;
	}
	
	// zde je vysledek 10
}